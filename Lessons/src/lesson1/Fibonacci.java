package lesson1;

import java.util.Scanner;

public class Fibonacci {
    public static void main(String []args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Введите количество елементов- ");
        int n;
        if(sc.hasNextInt()) {
            n = sc.nextInt();
            if (n==0) {
                System.out.print("Последовательность Фибоначчи не может состоять из 0 елементов-");
            }else{
                if (n==1) {
                    System.out.print("Ваша последовательность Фибоначчи-"+"0");
                }else{
                    long array[] = new long[n];
                    array[0] = 0;
                    array[1] = 1;
                    System.out.print("Ваша последовательность Фибоначчи -");
                    System.out.print(array[0] + " " + array[1]);
                    for (int i = 2; i < n; i++) {
                        array[i] = array[i - 1] + array[i - 2];
                        System.out.print(" " + array[i]);
                    }
                    System.out.println();
                    System.out.print("Состоит из "+array.length+" елемента");
                }
            }
        }else{
            System.out.print("Вы ввели не целое число");
        }
    }
}