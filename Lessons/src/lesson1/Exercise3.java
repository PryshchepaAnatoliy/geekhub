package lesson1;

import java.util.Scanner;

public class Exercise3 {
    public static void methodIf(int x){
        String w;
        if(x==0){
            w="Ноль";
            System.out.print(w);
        }
        if(x==1){
            w="Один";
            System.out.print(w);
        }
        if(x==2){
            w="Два";
            System.out.print(w);
        }
        if(x==3){
            w="Три";
            System.out.print(w);
        }
        if(x==4){
            w="Четыри";
            System.out.print(w);
        }
        if(x==5){
            w="Пять";
            System.out.print(w);
        }
        if(x==6){
            w="Шесть";
            System.out.print(w);
        }
        if(x==7){
            w="Семь";
            System.out.print(w);
        }
        if(x==8){
            w="Восемь";
            System.out.print(w);
        }
        if(x==9){
            w="Девять";
            System.out.print(w);
        }
    }
    public static void methodSwitch(int x){
        switch (x) {
            case 0:
                System.out.println("Ноль");
                break;
            case 1:
                System.out.println("Один");
                break;
            case 2:
                System.out.println("Два");
                break;
            case 3:
                System.out.println("Три");
                break;
            case 4:
                System.out.println("Четыри");
                break;
            case 5:
                System.out.println("Пять");
                break;
            case 6:
                System.out.println("Шесть");
                break;
            case 7:
                System.out.println("Семь");
                break;
            case 8:
                System.out.println("Восемь");
                break;
            case 9:
                System.out.println("Девять");
                break;
        }
    }
    public static void methodArray(int x){
        String arr[] = new String[10];
        arr[0]="Ноль";
        arr[1]="Один";
        arr[2]="Два";
        arr[3]="Три";
        arr[4]="Четыри";
        arr[5]="Пять";
        arr[6]="Шесть";
        arr[7]="Семь";
        arr[8]="Восемь";
        arr[9]="Девять";
        System.out.println(arr[x]);
    }
    public static void main(String []args) {
        int n,s;
        Scanner sc = new Scanner(System.in);
        System.out.print("Введите цифру от 0 до 9:");
        if(sc.hasNextInt()) {
            n = sc.nextInt();
            if ((n<0)||(n>9)){
                System.out.println("Вы ввели число больше 9 или меньше 0, попробуйте еще раз.");
            }else{
                System.out.println("Каким способом вы хотите вывести заданную цифру в словесной форме?");
                System.out.println("1 - через оператор if else");
                System.out.println("2 - через оператор switch case");
                System.out.println("3 - через массив");
                if(sc.hasNextInt()){
                    s = sc.nextInt();
                    switch (s) {
                        case 1:
                            methodIf(n);
                            break;
                        case 2:
                            methodSwitch(n);
                           break;
                        case 3:
                            methodArray(n);
                            break;
                        default:
                            System.out.println("Вы ввели не верное число.");
                            break;
                    }
                }else{
                    System.out.println("Вы ввели не целое число или это вовсе не цифра.");
                }
            }
        }else{
            System.out.println("Вы ввели не целое число или это вовсе не цифра.");
        }
    }
}
