package lesson1;

import java.util.Scanner;

public class Factorial {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        long num;
        long res = 1;
        System.out.print("Введите целое число:");
        if(sc.hasNextInt()) {
            num = sc.nextInt();
            for (int i=1; i<=num; i++){
                res*=i;
            }
            System.out.println("Факториал "+num+"="+res);
        }else
            System.out.println("Вы ввели не целое число");
    }
}