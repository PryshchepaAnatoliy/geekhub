package storage;

import objects.Entity;
import objects.Ignore;

import java.lang.reflect.Field;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Implementation of {@link storage.Storage} that uses database as a storage for objects.
 * It uses simple object type names to define target table to save the object.
 * It uses reflection to access objects fields and retrieve data to map to database tables.
 * As an identifier it uses field id of {@link objects.Entity} class.
 * Could be created only with {@link java.sql.Connection} specified.
 */
public class DatabaseStorage implements Storage {
    private Connection connection;

    public DatabaseStorage(Connection connection) {
        this.connection = connection;
    }

    @Override
    public <T extends Entity> T get(Class<T> clazz, Integer id) throws Exception {
        //this method is fully implemented, no need to do anything, it's just an example
        String sql = "SELECT * FROM " + clazz.getSimpleName() + " WHERE id = " + id;
        try(Statement statement = connection.createStatement()) {
            List<T> result = extractResult(clazz, statement.executeQuery(sql));
            return result.isEmpty() ? null : result.get(0);
        }
    }

    @Override
    public <T extends Entity> List<T> list(Class<T> clazz) throws Exception {
        String sql = "SELECT * FROM " + clazz.getSimpleName();
        List<T> result= null;
        try(Statement statement = connection.createStatement()) {
            result = extractResult(clazz, statement.executeQuery(sql));
        }finally {
            return result;
        }
    }

    @Override
    public <T extends Entity> boolean delete(T entity) throws Exception {
        int a = 0;
        String sql = "DELETE FROM " + entity.getClass().getSimpleName() + " WHERE id = " + entity.getId();
        try(Statement statement = connection.createStatement()) {
             a = statement.executeUpdate(sql);
        }finally {
            if(a != 0){
                return true;
            }else{
                return false;
            }
        }
    }

    @Override
    public <T extends Entity> void save(T entity) throws Exception {
        Map<String, Object> data = prepareEntity(entity);
        String sql = null;
        StringBuffer sqlBuf = new StringBuffer();
        List<String> listKey = new ArrayList<String>();
        List<Object> listValue = new ArrayList<Object>();

        for (Map.Entry entry : data.entrySet()) {
            Object s = entry.getValue();
            //System.out.println(s+ "=s");
//            if (entry.getValue().toString() == "true"){
//                s = "1";
//                System.out.println(s+ "true");
//            }else if (entry.getValue().toString() == "false"){
//                s = "0";
//            }
            listKey.add(entry.getKey().toString());
            listValue.add(s);
        }
        String t= "?";
        for(int i =1; i<= listValue.size()-1; i++){
            t=t+",?";
        }
        String fields = myReplace(listKey.toString());
        String values = myReplace(listValue.toString());
        if (entity.isNew()) {
            for (Map.Entry entry : data.entrySet()) {
                sql = "INSERT INTO " + entity.getClass().getSimpleName() + " " + fields + " VALUES " + "("+t+")" + " ";
            }
            PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

            for (Object o : listValue){
                preparedStatement.setObject(listValue.indexOf(o) + 1, o);
            }
            preparedStatement.executeUpdate();
            try (ResultSet generatedKey = preparedStatement.getGeneratedKeys()) {
                if (generatedKey.next()) {
                    entity.setId(generatedKey.getInt(1));
                }
            }
           // sqlBuf.append("INSERT INTO ").append(entity.getClass().getSimpleName()).append(" ").append(fields).append("VALUES ").append(values).append(" ");
          //  sql = sqlBuf.toString();
         //   PreparedStatement pS = connection.prepareStatement(sql);
//            pS.executeUpdate(sql,Statement.RETURN_GENERATED_KEYS);
//            try (ResultSet generatedKey = pS.getGeneratedKeys()) {
//                if (generatedKey.next()) {
//                    entity.setId(generatedKey.getInt(1));
//                }
           // }
        } else {
            for (Map.Entry entry : data.entrySet()) {

                sql = "UPDATE " + entity.getClass().getSimpleName()+" " + "SET "+ entry.getKey().toString() + " = ? WHERE id = " + entity.getId();
                PreparedStatement preparedStatement = connection.prepareStatement(sql);
                preparedStatement.setObject(1, entry.getValue());
                preparedStatement.executeUpdate();
            }
//            Statement statement = connection.createStatement();
//            for (Map.Entry entry : data.entrySet()) {
//                String s = entry.getValue().toString();
//                if (entry.getValue().toString() == "true"){
//                    s = "1";
//                }else if (entry.getValue().toString() == "false"){
//                    s = "0";
//                }
//                sql = "UPDATE " + entity.getClass().getSimpleName()+" " + "SET "+entry.getKey().toString() + " = " + "'"+s+"'"+ " WHERE id = " + entity.getId();
//                int rowsAffected = statement.executeUpdate(sql);
               // }
            }
        }

    private <T extends Entity> Map<String, Object> prepareEntity(T entity) throws Exception {
        Map<String, Object> data = new HashMap<>();
        Class clazz = entity.getClass();
        Field[] fields = clazz.getDeclaredFields();
        for (Field field: fields){
            field.setAccessible(true);
            if (field.getDeclaredAnnotation(Ignore.class) != null) {
                continue;
            }
            data.put(field.getName().toString(), field.get(entity));
            field.setAccessible(false);
        }
        return data;
    }

    //creates list of new instances of clazz by using data from resultset
    private <T extends Entity> List<T> extractResult(Class<T> clazz, ResultSet resultSet) throws Exception {
        List<T> newListInstances = new ArrayList<>();
        Field[] fields = clazz.getDeclaredFields();
        int i = 0;
        while (resultSet.next()) {
            newListInstances.add(clazz.newInstance());
            newListInstances.get(i).setId(resultSet.getInt("id"));
            for (Field field : fields) {
                if (field.getAnnotation(Ignore.class) != null) {
                    continue;
                }
                field.setAccessible(true);
                field.set(newListInstances.get(i), resultSet.getObject(field.getName()));
                field.setAccessible(false);
            }
            i++;
        }
        return newListInstances;
    }

    public String myReplace (String a){
        String newString;
        newString = a.replace("[","(");
        newString= newString.replace("]",")");
        return newString;
    }
}