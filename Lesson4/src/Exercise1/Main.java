package Exercise1;

import java.util.*;

/**
 * Created by Анатолий on 14.11.2014.
 */

public class Main {
    public static int s;
    public static void main(String[] args) {
        SetOperationsClass setOperations = new SetOperationsClass();
        Set<Integer> op = new HashSet<Integer>();
        Set<Integer> op1 = new HashSet<Integer>();
        Random rand = new Random();

        s = rand.nextInt(10);
        for(int i=0; i<s; i++){
            op.add(rand.nextInt(50));
        }
        s = rand.nextInt(10);
        for(int i=0; i<s; i++){
            op1.add(rand.nextInt(50));
       }

        StringBuilder stringBuilder = new StringBuilder();
        System.out.println(stringBuilder.append("multiplicity1:").append(op).append("\n").append("multiplicity2:").append(op1));
        System.out.println("Equals-" + setOperations.equals(op, op1));
        System.out.println("Union"+setOperations.union(op, op1));
        System.out.println("Subtract"+setOperations.subtract(op, op1));
        System.out.println("Intersect"+setOperations.intersect(op, op1));
        System.out.println("SymmetricSubtract"+setOperations.symmetricSubtract(op, op1));

    }
}
