package Exercise1;

import java.util.*;

public class SetOperationsClass implements SetOperations {

    @Override
    public boolean equals(Set a, Set b) {
        return a.containsAll(b) ? true : false;
    }

    @Override
    public Set union(Set a, Set b) {
        Set unionSet = new HashSet();
        unionSet.addAll(a);
        unionSet.addAll(b);
        return unionSet;
    }

    @Override
    public Set subtract(Set a, Set b) {
        Set subtractSet = new HashSet();
        subtractSet.addAll(a);
        subtractSet.removeAll(b);
        return subtractSet;
    }

    @Override
    public Set intersect(Set a, Set b) {
        Set intersectSet = new HashSet();
        for (Object n : a) {
            for (Object m : b){
                if (n.equals(m)) {
                    intersectSet.add(n);
                }
            }
        }return intersectSet;
    }

    @Override
    public Set symmetricSubtract(Set a, Set b) {
        Set symmetricSubtractSet = union(subtract(a,b),subtract(b,a));
        return symmetricSubtractSet;
    }
}
