package Exercise2;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Анатолий on 15.11.2014.
 */
public class Main {
    public static Scanner sc = new Scanner(System.in);
    public static SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");

    public static void main(String[] args) {
        TaskManagerClass taskManagerClass = new TaskManagerClass();
        boolean exit = true;
        while (exit) {
            System.out.println("__________Menu___________");
            System.out.println("1-Add task\n2-Remove task\n3-All categories\n4-Tasks by Category\n5-All tasks by categories\n6-Task for today\n7-Exit");
            int operation = Integer.parseInt(sc.nextLine());
            String category;
            switch (operation) {
                case 1:
                    System.out.println("__________Add task___________");
                    taskManagerClass.addTask(taskManagerClass.inputDate(),taskManagerClass.inputTask());
                    break;
                case 2:
                    System.out.println("__________Remove task_________");
                    Date date = new Date();
                    String dateString;
                    System.out.println("Input date.(FORMAT:DD.MM.YYYY)");
                    dateString = sc.nextLine();
                    try {
                        date = dateFormat.parse(dateString);
                    } catch (ParseException e) {
                        System.out.println("Invalid format.");
                        e.printStackTrace();
                    }
                    taskManagerClass.removeTask(date);
                    break;
                case 3:
                    System.out.println("_________All categories________");
                    Collection<String> categories = taskManagerClass.getCategories();
                    System.out.println("All categories : ");
                    System.out.println(categories);
                    break;
                case 4:
                    System.out.println("__________Tasks by category_______");
                    System.out.println("Input category: ");
                    category = sc.nextLine();
                    List<Task> listOfTasksByCategory = taskManagerClass.getTasksByCategory(category);
                    for(Task o : listOfTasksByCategory){
                        o.getInformationForTask();
                    }
                    break;
                case 5:
                    System.out.println("__________All tasks by categories_______");
                    List<Task> listOfTasksByCategories = taskManagerClass.getTasksByCategories();
                    for(Task o : listOfTasksByCategories){
                        o.getInformationForTask();
                    }
                    break;
                case 6:
                    System.out.println("_________Tasks for today!_________");
                    taskManagerClass.getTasksForToday();
                    break;
                case 7:
                    System.out.println("Exit done!");
                    exit = false;
                    break;
            }
        }
    }
}