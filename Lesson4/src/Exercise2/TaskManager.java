package Exercise2;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

public interface TaskManager {

    public void addTask(Date date, Task task);

    public void removeTask(Date date);

    public Collection<String> getCategories();

    public List<Task> getTasksByCategories();

    public List<Task> getTasksByCategory(String category);

    public void getTasksForToday();
}
