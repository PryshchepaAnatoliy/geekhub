package Exercise2;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Анатолий on 15.11.2014.
 */
public class TaskManagerClass implements TaskManager {

    Map<Date, Task> tasks = new HashMap<Date, Task>();
    Set<String> categoryList = new HashSet<String>();
    Scanner sc = new Scanner(System.in);
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");

    public Date inputDate() {
        Date date = new Date();
        String dateString;
        System.out.println("Input date.(FORMAT:DD.MM.YYYY)");
        dateString = sc.nextLine();

        try {
            date = dateFormat.parse(dateString);
        } catch (ParseException e) {
            System.out.println("Invalid format of date.");
        }
        if (dateFormat.format(date).equals(dateString) == false) {
            System.out.println("Invalid date.");
        } else {
            System.out.println("Valid date.");
        }
        return date;
    }

    public Task inputTask() {
        Task task = new Task();
        System.out.println("Input category.");
        String category = sc.nextLine();

        categoryList.add(category);

        System.out.println("Input description.");
        String description = sc.nextLine();

        task.setCategory(category);
        task.setDescription(description);

        return task;
    }


    @Override
    public void addTask(Date date, Task task) {
      //  Date date = inputDate();
//        Task task = new Task();
//        System.out.println("Input category.");
//        String category = sc.nextLine();
//
//        categoryList.add(category);
//
//        System.out.println("Input description.");
//        String description = sc.nextLine();
//
//        task.setCategory(category);
//        task.setDescription(description);
        tasks.put(date, task);
    }

    @Override
    public void removeTask(Date date) {
        tasks.remove(date);
    }

    @Override
    public Collection<String> getCategories() {
        Collection<String> list = categoryList;
        return list;
    }

    @Override
    public List<Task> getTasksByCategories() {
        List<Task> TasksByCategories = new ArrayList<Task>();
        List<Task> listTask = new ArrayList<Task>(tasks.values());

        for (Task task : listTask) {
            for (String category : categoryList) {
                if (category.equals(task.getCategory())) {
                    TasksByCategories.add(task);
                }
            }
        }
        Collections.sort(TasksByCategories);
        return TasksByCategories;
    }

    @Override
    public List<Task> getTasksByCategory(String category) {
        List<Task> tasksByCategory = new ArrayList<Task>();
        for(Task o : tasks.values()){
            Task task = o;
            if (task.getCategory().equals(category)) {
                tasksByCategory.add(task);
            }
        }
         return tasksByCategory;
    }

    @Override
    public void getTasksForToday() {
        ArrayList<Task> getTasksForToday = new ArrayList<Task>();
        Date currentDate  = new Date();
        String newString = new SimpleDateFormat("dd.MM.yyyy").format(currentDate);
        try {
            currentDate = new SimpleDateFormat("dd.MM.yyyy").parse(newString);
        } catch (ParseException e) {
            e.printStackTrace();
            System.out.println("Invalid format of date.");
        }
        for (Date a : tasks.keySet()){
            if (a.equals(currentDate) == true){
                getTasksForToday.add(tasks.get(currentDate));
                for(Task j : getTasksForToday){
                    j.getInformationForTask();
                }
            }
        }
    }
}
