package Exercise2;

/**
 * Created by Анатолий on 14.11.2014.
 */
public class Task implements Comparable<Task>{
    private String category;
    private String description;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void getInformationForTask(){
        StringBuffer s = new StringBuffer();
        System.out.println(s.append("Category-").append(category).append("\n").append("Description-")
                           .append(description).append("\n").append("---------------"));
    }

    @Override
    public int compareTo(Task task) {
        return category.compareTo(task.category);
    }
}
