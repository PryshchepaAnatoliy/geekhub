import java.lang.reflect.Field;

/**
 * Created by Анатолий on 07.12.2014.
 */
public class BeanComparator {
    public void clazzComparator(Object obj,Object obj1){
        Class clazz = obj.getClass();
        Class clazz1 = obj1.getClass();
        if(clazz != clazz1){
            System.out.println("Это разные класы!");
        }else{
            System.out.println("Field | A | B | Match");
            Field[] arrFieldsСlazz = clazz.getDeclaredFields();
            Field.setAccessible(arrFieldsСlazz, true);
            try{
                for (Field field : arrFieldsСlazz) {
                        System.out.println(field.getName() + "|" + field.get(obj) + "|"
                                + field.get(obj1) + "|" + field.get(obj).equals(field.get(obj1)));
                }
            }catch (IllegalArgumentException | IllegalAccessException e) {
                System.out.println(e.getLocalizedMessage());
            }
        }
    }
    public static void main(String[] args) {
        BeanComparator b= new BeanComparator();
        Cat cat = new Cat("Blue",3,4,"38mm");
        Cat cat1 = new Cat("Blue",3,4,"36mm");
        b.clazzComparator(cat, cat1);
    }

}
