/**
 * Created by Анатолий on 07.12.2014.
 */
public class Cat {
    private String color;
    private int age;
    private int legCount;
    private String furLength;

    Cat(String color, int age, int legCount, String furLength){
        this.color=color;
        this.age=age;
        this.legCount=legCount;
        this.furLength=furLength;
    }
    Cat(){

    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getLegCount() {
        return legCount;
    }

    public void setLegCount(int legCount) {
        this.legCount = legCount;
    }

    public String getFurLength() {
        return furLength;
    }

    public void setFurLength(String furLength) {
        this.furLength = furLength;
    }
}
