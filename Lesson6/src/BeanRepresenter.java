import java.lang.reflect.Field;

public class BeanRepresenter {

    public void writeInfoClass(Object obj) {
        Class clazz = obj.getClass();
        System.out.println("Имя класа - "+clazz.getName());
        Field[] arrFields = clazz.getDeclaredFields();
        Field.setAccessible(arrFields, true);
        try{
            for (Field field : arrFields) {
                if(!field.getType().isInstance(toString()) && !field.getType().isPrimitive()){
                    writeInfoClass(field.get(obj));
                } else {
                    System.out.println(field.getName() + " " + field.get(obj));
                }
            }
        }catch (IllegalArgumentException | IllegalAccessException e) {
            System.out.println(e.getLocalizedMessage());
        }
    }

    public static void main(String[] args) {
        BeanRepresenter b= new BeanRepresenter();
        Object cat = new Cat("Blue",3,4,"36mm");
        b.writeInfoClass(cat);
    }
}
