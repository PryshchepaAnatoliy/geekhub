/**
 * Created by Анатолий on 08.12.2014.
 */
public class Main {
    public static void main(String[] args) {
        Cat cat = new Cat("Blue",3,4,"36mm");
        CloneCreator creator= new CloneCreator();
        BeanRepresenter beanRepresenter= new BeanRepresenter();
        BeanComparator beanComparator= new BeanComparator();

        beanRepresenter.writeInfoClass(cat);
        //Object cat1 = creator.cloneCreator(cat);
        Cat cat1 = new Cat("Blue",3,4,"34mm");
        beanComparator.clazzComparator(cat, cat1);
    }
}
