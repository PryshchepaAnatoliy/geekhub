import java.lang.reflect.Constructor;
import java.lang.reflect.Field;

/**
 * Created by Анатолий on 08.12.2014.
 */
public class CloneCreator<E> {

    public E cloneCreator (E inObj){
        Class inClazz = inObj.getClass();
        Field[] inFields = inClazz.getDeclaredFields();
        E outObj = null;

        Constructor[] ctors = inClazz.getDeclaredConstructors();
        Constructor ctor = null;

        for (int i = 0; i < ctors.length; i++) {
            ctor = ctors[i];
            if (ctor.getGenericParameterTypes().length == 0)
                break;
        }
        try {
            outObj = (E) inClazz.newInstance();
            Class<?> outClazz = outObj.getClass();
            Field[] outFields = outClazz.getDeclaredFields();;
            Field.setAccessible(inFields, true);
            Field.setAccessible(outFields, true);
            for (int i = 0; i < inFields.length; i++){
                Object obj = inFields[i].get(inObj);
                outFields[i].set(outObj,obj);
            }
            System.out.println("Имя класа - "+inClazz.getName());
            System.out.println("Оригинал");
            for (Field field : inFields) {
                System.out.println(field.getName() + " " + field.get(inObj));
            }
            System.out.println("");
            System.out.println("Имя копии класа - "+outClazz.getName());
            System.out.println("Копия");
            for (Field field : outFields) {
                System.out.println(field.getName() + " " + field.get(outObj));
            }
        } catch (IllegalAccessException | InstantiationException e) {
            e.printStackTrace();
        }catch (NullPointerException e) {
            System.out.println("Field is null");
        }
        return outObj;
    }
    public static void main(String[] args) {
        CloneCreator b= new CloneCreator();
        Cat cat = new Cat("Blue",3,4,"36mm");
        Cat cat1 = (Cat) b.cloneCreator(cat);
    }
}
