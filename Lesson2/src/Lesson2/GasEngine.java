package Lesson2;

public class GasEngine implements ForceProvider {

    @Override
    public int createForce(int speed) {
        int engineSpeed = speed * 10;
        return engineSpeed;
    }
}
