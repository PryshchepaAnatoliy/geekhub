package Lesson2;

import java.util.Scanner;

public class SolarPoweredCar extends Vehicle {

    SolarBattery battery = new SolarBattery(28800,7200);
    ElectricEngine electricEngine = new ElectricEngine();
    Wheel wheel = new Wheel();

    public double currentEnergy = battery.refuel();
    public double startEnergy;
    public double distance = 0;
    public double residueEnergy = currentEnergy;
    public int engineSpeed;
    private int rotationWheels;

    public double getCurrentEnergy() {
        return startEnergy;
    }

    public void setCurrentEnergy(double startFuel) {
        this.startEnergy = startFuel;
    }

    public void setEngineSpeed(int engineSpeed) {
        this.engineSpeed = engineSpeed;
    }

    public int getEngineSpeed() {
        return engineSpeed;
    }

    public void setRotationWheels(int rotationWheels) {
        this.rotationWheels = rotationWheels;
    }

    public int getRotationWheels() {
        return rotationWheels;
    }

    public void distance(){
        if (currentEnergy <= 0){
            distance = this.distance + getCurrentEnergy() / 100 ;
            currentEnergy = 0;
            System.out.println("Закончился бензин!");
            brake();

        }else {
            residueEnergy = getEngineSpeed() * 3;
            distance = this.distance + residueEnergy / 100;
        }
    }

    public void setStatusOfCar(int speed, int engineSpeed, int rotationWheels, Course course){
        setSpeed(speed);
        setEngineSpeed(engineSpeed);
        setRotationWheels(rotationWheels);
        setCourse(course);

    }
    public void getStatusOfCar(){
        System.out.println("Текущие показатели автомобиля:");
        System.out.println("Скорость -" + getSpeed()+ "км/час");
        System.out.println("Обороты двигателя -"+ getEngineSpeed());
        System.out.println("Обороты колес -" + getRotationWheels());
        System.out.println("Курс -" + getCourse());
        System.out.println("Текущие состояние акамулятора -" + currentEnergy + "Дж.");
        System.out.println("Пройденная дистанция -" + distance + "км.");
    }

    @Override
    public void starting(){
        turn();
        int speed;
        System.out.println("С какой скоростью вы хотите двигатся?\n" +
                           "Не забывайте что максимальная скорость вашего автомобиля 120км/час.Рекомендуемая 60км/час");
        Scanner sc = new Scanner(System.in);
        if (sc.hasNextInt()) {
            speed = sc.nextInt();
            if (speed > 120) {
                speed=120;
                setStatusOfCar(speed, electricEngine.createForce(speed), wheel.usingForce(electricEngine.createForce(speed)), getCourse());
                currentEnergy -= getEngineSpeed() * 3;
                setCurrentEnergy(currentEnergy);
                distance();
                getStatusOfCar();
                System.out.println("Я вижу вы забыли что максимальная скорость вашего автомобиля 120км/час");
            }
            if (speed <= 0){
                speed=60;
                setStatusOfCar(speed, electricEngine.createForce(speed),  wheel.usingForce(electricEngine.createForce(speed)), getCourse());
                currentEnergy -= getEngineSpeed() * 3;
                setCurrentEnergy(currentEnergy);
                distance();
                getStatusOfCar();
                System.out.println("Вы ввели не верную скорость!\nПо этому я установил рекомендуемую скорость " + speed + "км/час");
            }else {
                setStatusOfCar(speed, electricEngine.createForce(speed),  wheel.usingForce(electricEngine.createForce(speed)), getCourse());
                currentEnergy -= getEngineSpeed() * 3;
                setCurrentEnergy(currentEnergy);
                distance();
                getStatusOfCar();
                System.out.println("Сейчас ваша скорость - " + speed + "км/час");
            }
        }else {
            speed=60;
            setStatusOfCar(speed, electricEngine.createForce(speed),  wheel.usingForce(electricEngine.createForce(speed)), getCourse());
            currentEnergy -= getEngineSpeed() * 3;
            setCurrentEnergy(currentEnergy);
            distance();
            getStatusOfCar();
            System.out.println("Вы ввели не верное число или это вовсе не число.");
        }
        accelerate();
    }

    @Override
    public void accelerate() {
        int acc;
        turn();
        System.out.println("Ускорение!\nК какой скорости вы хотите ускорится или понизится?");
        Scanner sc = new Scanner(System.in);
        if (sc.hasNextInt()) {
            acc = sc.nextInt();
            if (acc > 180) {
                acc = 180;
                System.out.println("Уже больше некуда ускорятся");
                setStatusOfCar(acc, electricEngine.createForce(acc), wheel.usingForce(electricEngine.createForce(acc)), getCourse());
                currentEnergy -= getEngineSpeed() * 3;
                brake();
            }
            if (acc < 0) {
                acc = 180;
                System.out.println("Вы не можите ускориться на заданую скорость!"+
                                   "По этому я установил рекомендуемую скорость " + acc + "км/час");
                setStatusOfCar(acc, electricEngine.createForce(acc), wheel.usingForce(electricEngine.createForce(acc)), getCourse());
                currentEnergy -= getEngineSpeed() * 3;
                distance();
            }
            if (acc == 0) {
                setStatusOfCar(0, 0, 0, getCourse());
                currentEnergy -= getEngineSpeed() * 3;
                distance();
                getStatusOfCar();
            }
            else{
                setStatusOfCar(acc, electricEngine.createForce(acc), wheel.usingForce(electricEngine.createForce(acc)), getCourse());
                currentEnergy -= getEngineSpeed() * 3;
                distance();
                getStatusOfCar();
                brake();
            }
        }else {
            acc = 60;
            System.out.println("Вы ввели не верное число или это вовсе не число.\n" +
                               "По этому я установил рекомендуемую скорость " + acc + "км/час");
            setStatusOfCar(acc, electricEngine.createForce(acc), wheel.usingForce(electricEngine.createForce(acc)), getCourse());
            currentEnergy = this.currentEnergy - getEngineSpeed() * 3;
            distance();
            getStatusOfCar();
            brake();
        }
    }

    @Override
    public void turn() {
        int s;
        System.out.println("Введите курс куда вы хотите поехать:");
        System.out.println("1-Север");
        System.out.println("2-Юг");
        System.out.println("3-Запад");
        System.out.println("4-Восток");
        Scanner sc = new Scanner(System.in);
        if(sc.hasNextInt()){
            s = sc.nextInt();
            switch (s) {
                case 1:
                    setCourse(Course.NORTH);
                    break;
                case 2:
                    setCourse(Course.SOUTH);
                    break;
                case 3:
                    setCourse(Course.WEST);
                    break;
                case 4:
                    setCourse(Course.EAST);
                    break;
                default:
                    setCourse(Course.NORTH);
                    System.out.println("Вы ввели не верное число, по этому я посмел поставить курс на север");
                    break;
            }
        }else{
            setCourse(Course.NORTH);
            System.out.println("Вы ввели не верное число, по этому я посмел поставить курс на север");
        }
    }

    @Override
    public void brake() {
        int s;
        if((getSpeed() == 0)||(currentEnergy <= 0)){
            setStatusOfCar(0, 0, 0, getCourse());
        }else{
            System.out.println("Остановить автомобиль?\n1-Да \n2-Нет");
            Scanner sc = new Scanner(System.in);
            if(sc.hasNextInt()) {
                s = sc.nextInt();
                switch (s) {
                    case 1:
                        System.out.println("Машина стоит!");
                        setStatusOfCar(0, 0, 0, getCourse());
                        getStatusOfCar();
                        break;
                    case 2:
                        System.out.println("Вы продолжаете двежение!");
                        accelerate();
                        getStatusOfCar();
                        break;
                    default:
                        System.out.println("Машина стоит!");
                        setStatusOfCar(0, 0, 0, getCourse());
                        getStatusOfCar();
                        break;
                }
            }
        }
    }
}
