package Lesson2;

/**
 * Created by Анатолий on 25.10.2014.
 */
public interface Driveable {
    public void accelerate();
    public void brake();
    public void turn();
    public void starting();
}
