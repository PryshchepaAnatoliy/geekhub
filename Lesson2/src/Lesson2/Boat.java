package Lesson2;

import java.util.Scanner;

/**
 * Created by Анатолий on 25.10.2014.
 */
public class Boat extends Vehicle {
    GasEngine gasEngine = new GasEngine();
    Propeller propeller = new Propeller();
    GasTank tank = new GasTank(20,5);

    public double startFuel;
    public double currentFuel = tank.refuel();
    public double distance = 0;
    public double residueFuel = currentFuel;
    public int engineSpeed;
    private int rotationWheels;

    public double getCurrentFuel() {
        return startFuel;
    }

    public void setCurrentFuel(double startFuel) {
        this.startFuel = startFuel;
    }

    public int getEngineSpeed() {
        return engineSpeed;
    }

    public void setEngineSpeed(int engineSpeed) {
        this.engineSpeed = engineSpeed;
    }

    public void setRotationWheels(int rotationWheels) {
        this.rotationWheels = rotationWheels;
    }

    public int getRotationWheels() {
        return rotationWheels;
    }


    public void distance(){
        if (currentFuel<=0){
            distance = this.distance + getCurrentFuel() * 15;
            currentFuel = 0;
            brake();
            System.out.println("Закончился бензин!");
        }else {
            residueFuel = (int) (getEngineSpeed() * 0.01);
            distance = this.distance + residueFuel * 15;
        }
    }

    public void setStatusOfCar(int speed, int engineSpeed, int rotationWheels, Course course){
        setSpeed(speed);
        setEngineSpeed(engineSpeed);
        setRotationWheels(rotationWheels);
        setCourse(course);

    }
    public void getStatusOfCar(){
        System.out.println("Текущие показатели лодки:");
        System.out.println("Скорость -" + getSpeed() + "км/час");
        System.out.println("Обороты двигателя -"+ getEngineSpeed());
        System.out.println("Обороты пропеллера -" + getRotationWheels());
        System.out.println("Курс -:" + getCourse());
        System.out.println("Текущие состояние бака -" + currentFuel + "л.");
        System.out.println("Пройденная дистанция -" + distance + "км");
    }

    @Override
    public void starting(){
        turn();
        int speed;
        System.out.println("С какой скоростью вы хотите двигатся?\n" +
                "Не забывайте что максимальная скорость вашей лодки 70км/час.Рекомендуемая 20км/час");
        Scanner sc = new Scanner(System.in);
        if (sc.hasNextInt()) {
            speed = sc.nextInt();
            if (speed > 70) {
                speed=70;
                setStatusOfCar(speed, gasEngine.createForce(speed), propeller.usingForce(gasEngine.createForce(speed)), getCourse());
                currentFuel -= (int) (getEngineSpeed() * 0.01);
                setCurrentFuel(currentFuel);
                distance();
                getStatusOfCar();
                System.out.println("Я вижу вы забыли что максимальная скорость вашей лодки 70км/час");
            }
            if (speed <= 0){
                speed=20;
                setStatusOfCar(speed, gasEngine.createForce(speed), propeller.usingForce(gasEngine.createForce(speed)), getCourse());
                currentFuel -= (int) (getEngineSpeed() * 0.01);
                setCurrentFuel(currentFuel);
                distance();
                getStatusOfCar();
                System.out.println("Вы ввели не верную скорость!\nПо этому я установил рекомендуемую скорость " + speed + "км/час");
            }else {
                setStatusOfCar(speed, gasEngine.createForce(speed), propeller.usingForce(gasEngine.createForce(speed)), getCourse());
                currentFuel -= (int) (getEngineSpeed() * 0.01);
                setCurrentFuel(currentFuel);
                distance();
                getStatusOfCar();
                System.out.println("Сейчас ваша скорость - " + speed + "км/час");
            }
        }else {
            speed=60;
            setStatusOfCar(speed, gasEngine.createForce(speed), propeller.usingForce(gasEngine.createForce(speed)), getCourse());
            currentFuel -= (int) (getEngineSpeed() * 0.01);
            setCurrentFuel(currentFuel);
            distance();
            getStatusOfCar();
            System.out.println("Вы ввели не верное число или это вовсе не число.");
        }
        accelerate();
    }

    @Override
    public void accelerate() {
        int acc;
        turn();
        System.out.println("Ускорение!\nК какой скорости вы хотите ускорится или понизится?");
        Scanner sc = new Scanner(System.in);
        if (sc.hasNextInt()) {
            acc = sc.nextInt();
            if (acc > 70) {
                acc = 70;
                System.out.println("Уже больше некуда ускорятся");
                setStatusOfCar(acc, gasEngine.createForce(acc), propeller.usingForce(gasEngine.createForce(acc)), getCourse());
                currentFuel -= getEngineSpeed() * 0.01;
                distance();
                //brake();
            }
            if (acc < 0) {
                acc = 20;
                System.out.println("Вы не можите ускориться на заданую скорость!"+
                        "По этому я установил рекомендуемую скорость " + acc + "км/час");
                setStatusOfCar(acc, gasEngine.createForce(acc), propeller.usingForce(gasEngine.createForce(acc)), getCourse());
                currentFuel -= getEngineSpeed() * 0.01;
                distance();
            }
            if (acc == 0) {
                setStatusOfCar(0, 0, 0, getCourse());
                currentFuel -= getEngineSpeed() * 0.01;
                distance();
                getStatusOfCar();
            }else{
                setStatusOfCar(acc, gasEngine.createForce(acc), propeller.usingForce(gasEngine.createForce(acc)), getCourse());
                currentFuel -= getEngineSpeed() * 0.01;
                distance();
                getStatusOfCar();
                brake();
            }
        }else {
            acc = 60;
            System.out.println("Вы ввели не верное число или это вовсе не число.\n" +
                    "По этому я установил рекомендуемую скорость " + acc + "км/час");
            setStatusOfCar(acc, gasEngine.createForce(acc), propeller.usingForce(gasEngine.createForce(acc)), getCourse());
            currentFuel = this.currentFuel - (int) (getEngineSpeed() * 0.01);
            distance();
            getStatusOfCar();
            brake();
        }
    }

    @Override
    public void turn() {
        int s;
        System.out.println("Введите курс куда вы хотите поехать:");
        System.out.println("1-Север");
        System.out.println("2-Юг");
        System.out.println("3-Запад");
        System.out.println("4-Восток");
        Scanner sc = new Scanner(System.in);
        if(sc.hasNextInt()){
            s = sc.nextInt();
            switch (s) {
                case 1:
                    setCourse(Course.NORTH);
                    break;
                case 2:
                    setCourse(Course.SOUTH);
                    break;
                case 3:
                    setCourse(Course.WEST);
                    break;
                case 4:
                    setCourse(Course.EAST);
                    break;
                default:
                    setCourse(Course.NORTH);
                    System.out.println("Вы ввели не верное число, по этому я посмел поставить курс на север");
                    break;
            }
        }else{
            setCourse(Course.NORTH);
            System.out.println("Вы ввели не верное число, по этому я посмел поставить курс на север");
        }
    }

    @Override
    public void brake() {
        int s;
        if((getSpeed() == 0)||(currentFuel <= 0)){
            setStatusOfCar(0, 0, 0, getCourse());
        }else{
            System.out.println("Остановить лодку?\n1-Да \n2-Нет");
            Scanner sc = new Scanner(System.in);
            if(sc.hasNextInt()) {
                s = sc.nextInt();
                switch (s) {
                    case 1:
                        System.out.println("Лодка стоит!");
                        setStatusOfCar(0, 0, 0, getCourse());
                        getStatusOfCar();
                        break;
                    case 2:
                        System.out.println("Вы продолжаете двежение!");
                        accelerate();
                        getStatusOfCar();
                        break;
                    default:
                        System.out.println("Лодка стоит!");
                        setStatusOfCar(0, 0, 0, getCourse());
                        getStatusOfCar();
                        break;
                }
            }
        }
    }
}
