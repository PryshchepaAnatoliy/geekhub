package Lesson2;

import java.util.Scanner;

public class Main {
    public static void main(String []args){
        Driveable vehicle;
        int s;
        Scanner sc = new Scanner(System.in);
        System.out.println("На чем вы хотите путишествовать?");
        System.out.println("1 - Автомобиль на бензине");
        System.out.println("2 - Автомобиль на солничной енергии");
        System.out.println("3 - Моторная лодка");
        if(sc.hasNextInt()){
            s = sc.nextInt();
            switch (s) {
                case 1:
                    vehicle =  new PetrolCar();
                    vehicle.starting();
                    break;
                case 2:
                    vehicle = new SolarPoweredCar();
                    vehicle.starting();
                    break;
                case 3:
                    vehicle = new Boat();
                    vehicle.starting();
                    break;
                default:
                    System.out.println("Вы ввели не верное число.");
                    break;
            }
        }else{
            System.out.println("Вы ввели не целое число или это вовсе не цифра.");
        }
    }


}
