package Lesson2;

/**
 * Created by Анатолий on 28.10.2014.
 */
public class ElectricEngine implements ForceProvider {

    @Override
    public int createForce(int speed) {
        int engineSpeed = speed * 8;
        return engineSpeed;
    }
}
