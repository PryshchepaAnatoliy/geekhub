package Lesson2;

import java.util.Scanner;

/**
 * Created by Анатолий on 26.10.2014.
 */
public class GasTank implements EnergyProvider {
    private final int maxFuel;
    private double fuel;
    GasTank(int maxF, double f){
        this.maxFuel = maxF;
        this.fuel = f;
    }
    public double refuel() {
        System.out.println("Чтобы двигатся вам нужно заправиться, хотя у вас есть "+ fuel +"л." +
                           "\nНужноли дозаправится?\n1 - Да\n2 - Нет ");
        Scanner sc = new Scanner(System.in);
        if (sc.hasNextInt()) {
            int s = sc.nextInt();
            switch (s) {
                case 1:
                    float lit;
                    System.out.println("Сейчас в вашем баке - " + fuel + "л.");
                    System.out.println("На сколько вы хотите дозаправится?\nНе забывайте что у ваш бак на "+ maxFuel +"л.");
                    if (sc.hasNextInt()) {
                        lit = sc.nextInt();
                        if (lit < 0) {
                            System.out.println("Вы ввели не верный литраж!" +
                                               "\nПо этому вы не заправились, Сейчас в вашем баке - " + fuel + "л.");
                            return fuel;
                        }
                        fuel += lit;
                        if (fuel > maxFuel) {
                            double remainder = fuel - maxFuel;
                            fuel = maxFuel;
                            System.out.println("Я вижу вы забыли что максимальное содержание бака - " + maxFuel + "л." +
                                                "\nПо этому бензин который не влез в бак вы оставили, он становит " + remainder + "л. " +
                                                "\nСейчас в вашем баке - " + fuel + "л.");
                        } else {
                            System.out.println("Вы заправились на " + lit + "л. Сейчас в вашем баке - " + fuel + "л.");
                        }
                    } else {
                        System.out.print("Вы ввели не верный литраж. Сейчас в вашем баке - " + fuel + "л.");
                    }
                    return fuel;
                case 2:
                    System.out.println("Сейчас в вашем баке - " + fuel + "л.");
                    return fuel;
                default:
                    System.out.println("Вы ввели не верное число.");
                    break;
            }
        } else {
            System.out.println("Вы ввели не верное число или это вовсе не число.");
        }
        return fuel;
    }
}
