package Lesson2;

/**
 * Created by Анатолий on 28.10.2014.
 */
public class Propeller implements ForceAcceptor {

    ElectricEngine electricEngine= new ElectricEngine();

    @Override
    public int usingForce(int engineSpeed) {
        int rotationPropeller = electricEngine.createForce(engineSpeed) * 10;
        return rotationPropeller;
    }
}
