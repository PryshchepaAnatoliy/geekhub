<%@ page language="java" contentType="text/html; charset=utf8" pageEncoding="utf8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page session="false"%>
<html>
<head>
  <title>Регистрация</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
  <link href="${pageContext.request.contextPath}/resources/css/blog-post.css" rel="stylesheet">
  <style type="text/css">
    #wrap{
      background-image: -ms-linear-gradient(top, #FFFFFF 0%, #D3D8E8 100%);
      /* Mozilla Firefox */
      background-image: -moz-linear-gradient(top, #FFFFFF 0%, #D3D8E8 100%);
      /* Opera */
      background-image: -o-linear-gradient(top, #FFFFFF 0%, #D3D8E8 100%);
      /* Webkit (Safari/Chrome 10) */
      background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0, #FFFFFF), color-stop(1, #D3D8E8));
      /* Webkit (Chrome 11+) */
      background-image: -webkit-linear-gradient(top, #FFFFFF 0%, #D3D8E8 100%);
      /* W3C Markup, IE10 Release Preview */
      background-image: linear-gradient(to bottom, #FFFFFF 0%, #D3D8E8 100%);
      background-repeat: no-repeat;
      background-attachment: fixed;
    }
    legend{
      color:#141823;
      font-size:25px;
      font-weight:bold;
    }
    .signup-btn {
      background: #79bc64;
      background-image: -webkit-linear-gradient(top, #79bc64, #578843);
      background-image: -moz-linear-gradient(top, #79bc64, #578843);
      background-image: -ms-linear-gradient(top, #79bc64, #578843);
      background-image: -o-linear-gradient(top, #79bc64, #578843);
      background-image: linear-gradient(to bottom, #79bc64, #578843);
      /*-webkit-border-radius: 4;*/
      /*-moz-border-radius: 4;*/
      border-radius: 4px;
      text-shadow: 0px 1px 0px #898a88;
      -webkit-box-shadow: 0px 0px 0px #a4e388;
      -moz-box-shadow: 0px 0px 0px #a4e388;
      box-shadow: 0px 0px 0px #a4e388;
      font-family: Arial;
      color: #ffffff;
      font-size: 20px;
      padding: 10px 20px 10px 20px;
      border: solid #3b6e22  1px;
      text-decoration: none;
    }

    .signup-btn:hover {
      background: #79bc64;
      background-image: -webkit-linear-gradient(top, #79bc64, #5e7056);
      background-image: -moz-linear-gradient(top, #79bc64, #5e7056);
      background-image: -ms-linear-gradient(top, #79bc64, #5e7056);
      background-image: -o-linear-gradient(top, #79bc64, #5e7056);
      background-image: linear-gradient(to bottom, #79bc64, #5e7056);
      text-decoration: none;
    }
    .navbar-default .navbar-brand{
      color:#fff;
      font-size:30px;
      font-weight:bold;
    }
    .form .form-control { margin-bottom: 10px; }
    @media (min-width:768px) {
      #home{
        margin-top:50px;
      }
      #home .slogan{
        color: #0e385f;
        line-height: 29px;
        font-weight:bold;
      }
    }
  </style>
</head>
<body>
<div class="container" id="wrap">
  <div class="row">
    <div class="col-md-6 col-md-offset-3">
      <form:form method="post"  modelAttribute="user" accept-charset="utf-8" class="form" role="form">   <legend>Регистрация</legend>
        <h4>It's free and always will be.</h4>
        <div class="row">
          <div class="col-xs-6 col-md-6">
            <input type="text" name="firstName" value="" class="form-control input-lg" placeholder="Имя"  />                        </div>
          <div class="col-xs-6 col-md-6">
            <input type="text" name="lastName" value="" class="form-control input-lg" placeholder="Фамилия"  />                        </div>
        </div>
        <input type="text" name="email" value="" class="form-control input-lg" placeholder="Ваш Email"  />
        <input type="password" name="password" value="" class="form-control input-lg" placeholder="Пароль"  />
        <input type="password" name="confirm_password" value="" class="form-control input-lg" placeholder="Подтвердите пароль"  />
        <label>Birth Date</label>
        <form:input path="userAvatar" enctype="multipart/form-data"
                    action="/upload" /><br/>
        <label>Стать: </label>                    <label class="radio-inline">
          <input type="radio" name="gender" value="Мужчина" id=male />Мужчина
        </label>
        <label class="radio-inline">
          <input type="radio" name="gender" value="Женщина" id=female />Женщина
        </label>
        <br />
        <div style="color:red"><form:errors path="*" cssClass="error" /></div>
        <button class="btn btn-lg btn-primary btn-block signup-btn" type="submit">
          Зарегистрироваться</button>
      </form:form>
    </div>
  </div>
</div>
</div>
<%--<form:form method="post"  modelAttribute="user">--%>
  <%--<h1> Регистрация </h1>--%>
  <%--<p>--%>
    <%--<label for="emailsignup" class="youmail" data-icon="E" > Ваш email</label>--%>
    <%--<input id="emailsignup" name="email" required="required" type="email" placeholder="mylogin@mail.com"/>--%>
  <%--</p>--%>
  <%--<p>--%>
    <%--<label for="password" class="youpasswd" data-icon="P">Ваш пароль  </label>--%>
    <%--<input id="password" name="password" required="required" type="password" placeholder="123456"/>--%>
  <%--</p>--%>

  <%--<label for="userAvatar">Attach Image(JPG file only)</label>--%>

  <%--&lt;%&ndash;<form:input path="userAvatar" enctype="multipart/form-data" type="file" /><br/>&ndash;%&gt;--%>
  <%--&lt;%&ndash;<p>&ndash;%&gt;--%>
  <%--&lt;%&ndash;&lt;%&ndash;<label for="passwordsignup_confirm" class="youpasswd" data-icon="P">Пожалуйста, подтвердите пароль  </label>&ndash;%&gt;&ndash;%&gt;--%>
  <%--&lt;%&ndash;&lt;%&ndash;<form:password id="passwordsignup_confirm" path="confirmPassword" required="required" type="password" />&ndash;%&gt;&ndash;%&gt;--%>
  <%--&lt;%&ndash;&lt;%&ndash;placeholder="eg. X8df!90EO"/>&ndash;%&gt;&ndash;%&gt;--%>
  <%--&lt;%&ndash;</p>&ndash;%&gt;--%>
  <%--<p >--%>
    <%--<input type="submit" value="Регистрация"/>--%>
  <%--</p>--%>
  <%--<div style="color:red"><form:errors path="*" cssClass="error" /></div>--%>
  <%--<p class="change_link">--%>
  <%--Вы уже зарегистрированы?--%>
  <%--<a href="<c:url value="login.jsp" />" class="to_register"> Тогда войдите </a>--%>
  <%--</p>--%>
<%--</form:form>--%>
</body>
</html>
