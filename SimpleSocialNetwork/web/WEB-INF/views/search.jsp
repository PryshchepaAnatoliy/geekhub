<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>
<html>
<head>
  <title>Поиск</title>

  <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
  <link href="${pageContext.request.contextPath}/resources/css/blog-post.css" rel="stylesheet">
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/profile/${sessionUserId}">Главная</a>
    </div>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li>
          <a href="/search">Поиск</a>
        </li>
        <li>
          <a href="#">Чат</a>
        </li>
        <li>
          <a href="#">Помощь</a>
        </li>
      </ul>
      <div class="navbar-collapse collapse">
        <div class="navbar-form navbar-right">
          <div class="form-group">
            <sec:authorize access="isAuthenticated()">
              <a class="btn btn-success" href="<c:url value="/logout" />">Bыйти</a>
            </sec:authorize>
          </div>
        </div>
      </div>
    </div>
  </div>
</nav>

<div class="container">
  <div class="row">
    <div class="col-sm-3">
      <ul class="nav">
        <li><a href="/profile/${sessionUserId}">Моя страница</a></li>
        <li><a href="#">Мои друзья</a></li>
        <li><a href="#">Мои настройки</a></li>
        <li><a href="#">1</a></li>
      </ul>
    </div>
    <div class="col-lg-8">
      <div class="row">
        <div class="col-md-6 col-md-offset-3">
          <form:form method="post"   accept-charset="utf-8" class="form" role="form">   <legend>Поиск</legend>
            <div class="row">
              <div class="col-xs-6 col-md-6">
                <input type="text" name="firstName" value="" class="form-control input-lg" placeholder="Имя"  />                        </div>
              <div class="col-xs-6 col-md-6">
                <input type="text" name="patronymic" value="" class="form-control input-lg" placeholder="Фамилия"  />                        </div>
            </div><br>
            <input type="text" name="city" value="" class="form-control input-lg" placeholder="Город"  /><br>
            <input type="text" name="country" class="form-control input-lg" value="" placeholder="Страна"  /><br>
            <button class="btn btn-lg btn-primary btn-block signup-btn" type="submit">Поиск</button>
          </form:form>
        </div>
      </div>

    </div>
  </div>
  <c:forEach items="${find}" var="user">
    <div class="container-fluid well span6">
      <div class="row-fluid">
        <div class="span2" >
          <div class="col-md-3 col-lg-3 " align="center"> <img alt="User Pic" src="${pageContext.request.contextPath}/resources/images/photo.png" class="img-circle"> </div>
          <div class=" col-md-9 col-lg-9 ">
            <h3><a href="/profile/${user.id}">${user.firstName} ${user.lastName} ${user.patronymic}</a></h3>

              <h5><strong>Город:</strong> ${user.city}</h5>
              <h5><strong>Страна:</strong> ${user.country}</h5>
              <h5><strong>Дата рождения:</strong> ${user.birthday}</h5>

          </div>
        </div>
      </div>
    </div>
  </c:forEach>
  </div>
</div>

</body>
</html>
