
<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>
<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>${byId.firstName} ${byId.patronymic}</title>

  <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
  <link href="${pageContext.request.contextPath}/resources/css/blog-post.css" rel="stylesheet">
  <link href="${pageContext.request.contextPath}/resources/css/profile.css" rel="stylesheet">
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
  <div class="container">
    <div class="navbar-header">

      <a class="navbar-brand" href="/profile/${sessionUserId}">Главная</a>
    </div>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li>
          <a href="/search">Поиск</a>
        </li>
        <li>
          <a href="#">Чат</a>
        </li>
        <li>
          <a href="#">Помощь</a>
        </li>
      </ul>
      <div class="navbar-collapse collapse">
        <div class="navbar-form navbar-right">
          <div class="form-group">
            <sec:authorize access="isAuthenticated()">
              <a class="btn btn-success" href="<c:url value="/logout" />">Bыйти</a>
            </sec:authorize>
          </div>
        </div>
      </div>
    </div>
  </div>
</nav>
<div class="container">
  <div class="row">
    <div class="col-sm-3">
      <ul class="nav">
        <li><a href="/profile/${sessionUserId}">Моя страница</a></li>
        <li><a href="#">Мои друзья</a></li>
        <li><a href="#">Мои сообщения</a></li>
        <li><a href="/editUser${byId.id}">Мои настройки</a></li>
      </ul>
    </div>


    <div class="col-lg-8">
      <div class="row">
        <div class="col-md-6 col-md-offset-3">

          <form:form class="form" method="POST" modelAttribute="user">
            <div class="row">
              <div class="col-xs-6 col-md-6">
                <%--<span>Ведите id:</span><form:input path="id"></form:input>--%>
                  <h1>Редактирование:</h1>

                  <label>
                    <span>Ведите Имя:</span><form:input path="firstName"></form:input>
                  </label>

                  <label>
                  <span>Ведите отчество:</span><form:input path="lastName"></form:input>
                </label>

                  <label>
                    <span>Ведите Фамилия:</span><form:input path="patronymic"></form:input>
                  </label>

                  <label>
                    <span>Ведите город:</span><form:input path="city"></form:input>
                  </label>

                  <label>
                    <span>Ведите страну:</span><form:input path="country"></form:input>
                  </label>

                  <label>
                    <span>Ведите телефон:</span><form:input path="phone"></form:input>
                  </label>

                  <label>
                    <span>Ведите адрес:</span><form:input path="address"></form:input>
                  </label>
                  <label>Стать: </label>                    <label class="radio-inline">
                    <input type="radio" name="gender" value="Мужчина" id=male />Мужчина
                  </label>
                  <label class="radio-inline">
                    <input type="radio" name="gender" value="Женщина" id=female />Женщина
                  </label>

                  <label>
                      <%--<span>Ведите описание</span><br><textarea id="feedback" name="description"></textarea>--%>
                      <%--<form:input path="description"></form:input>--%>
                    <input value="Изменить" type="submit" />
                  </label>
                <%--<input type="text" name="firstName" value="" class="form-control input-lg" placeholder="Имя"  />                        </div>--%>
              <div class="col-xs-6 col-md-6">
                <%--<input type="text" name="patronymic" value="" class="form-control input-lg" placeholder="Фамилия"  />                        </div>--%>
            </div><br>
            <%--<input type="text" name="city" value="" class="form-control input-lg" placeholder="Город"  /><br>--%>
            <%--<input type="text" name="country" class="form-control input-lg" value="" placeholder="Страна"  /><br>--%>
            <%--<button class="btn btn-lg btn-primary btn-block signup-btn" type="submit">Поиск</button>--%>
          </form:form>
        </div>
      </div>

    </div>
    </div>
</div>
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/jquery.js"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/bootstrap.min.js"></script>
</body>
</html>



<%--<html>--%>
<%--<head>--%>
  <%--<meta charset="utf-8">--%>
  <%--<meta http-equiv="X-UA-Compatible" content="IE=edge">--%>
  <%--<meta name="viewport" content="width=device-width, initial-scale=1">--%>
  <%--<meta name="description" content="">--%>
  <%--<meta name="author" content="">--%>

  <%--<title>${byId.firstName} ${byId.patronymic} </title>--%>
  <%--<link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">--%>
  <%--<link href="${pageContext.request.contextPath}/resources/css/blog-post.css" rel="stylesheet">--%>
<%--</head>--%>
<%--<body>--%>



<%--<form:form class="form" method="POST" modelAttribute="user">--%>
  <%--<div>--%>
    <%--<h1>Редактирование :</h1>--%>
    <%--<label>--%>
      <%--<span>Ведите id:</span><form:input path="id"></form:input>--%>
    <%--</label>--%>

    <%--<label>--%>
      <%--<span>Ведите Имя:</span><form:input path="firstName"></form:input>--%>
    <%--</label>--%>

    <%--<label>--%>
      <%--<span>Ведите отчество:</span><form:input path="lastName"></form:input>--%>
    <%--</label>--%>

    <%--<label>--%>
      <%--<span>Ведите город:</span><form:input path="city"></form:input>--%>
    <%--</label>--%>

    <%--<label>--%>
      <%--<span>Ведите страну:</span><form:input path="country"></form:input>--%>
    <%--</label>--%>

    <%--<label>--%>
      <%--<span>Ведите телефон:</span><form:input path="phone"></form:input>--%>
    <%--</label>--%>

    <%--<label>--%>
      <%--<span>Ведите адрес:</span><form:input path="address"></form:input>--%>
    <%--</label>--%>

    <%--<label>--%>
        <%--&lt;%&ndash;<span>Ведите описание</span><br><textarea id="feedback" name="description"></textarea>&ndash;%&gt;--%>
        <%--&lt;%&ndash;<form:input path="description"></form:input>&ndash;%&gt;--%>
      <%--<input value="Изменить" type="submit" />--%>
    <%--</label>--%>

  <%--</div>--%>
<%--</form:form>--%>
<%--</body>--%>
<%--</html>--%>
