<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title>Spring MVC - Hibernate File Upload to Database Demo</title>
</head>
<body>
<div align="center">
  <h1>Spring MVC - Hibernate File Upload to Database Demo</h1>
  <%--<form  action="download" enctype="multipart/form-data" method="GET">--%>
    <%--<table border="0">--%>
      <%--<tr>--%>
        <%--<td>Pick file #1:</td>--%>
        <%--<td><inpute type="file" name="myfile" /></td>--%>
      <%--</tr>--%>
      <%--<tr>--%>
        <%--<td colspan="2" align="center"><input type="submit" value="Upload" /></td>--%>
      <%--</tr>--%>
    <%--</table>--%>
  <%--</form>--%>

  <form:form method="post" action="download"
             commandName="multiFileUpload" enctype="multipart/form-data">

    <h3>Click Add More Files button to add files</h3>
    <%--<input id="addMoreFile" type="button" value="Add More Files" /><br /><br />--%>
    <input id="myfile" type="file" name="myfile" />
    <input type="submit" value="Upload Files" />
  </form:form>
</div>
</body>
</html>
