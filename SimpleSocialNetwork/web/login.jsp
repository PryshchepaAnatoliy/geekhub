<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <title>Войти в систему</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/resources/css/blog-post.css" rel="stylesheet">

    <style type="text/css">
        .panel-heading {
            padding: 5px 15px;
        }

        .panel-footer {
            padding: 1px 15px;
            color: #A0A0A0;
        }

        .profile-img {
            width: 96px;
            height: 96px;
            margin: 0 auto 10px;
            display: block;
            -moz-border-radius: 50%;
            -webkit-border-radius: 50%;
            border-radius: 50%;
        }
    </style>
</head>
<body>
<div class="container" style="margin-top:40px">
    <div class="row">
        <div class="col-sm-6 col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <strong>Войти на сайт</strong>
                </div>
                <div class="panel-body">
                    <form role="form" method="POST" action="<c:url value="/j_spring_security_check" />" >
                        <fieldset>
                            <div class="row">
                                <div class="center-block">
                                    <img class="profile-img"
                                         src="${pageContext.request.contextPath}/resources/images/photo.png" alt="">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-md-10  col-md-offset-1 ">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"></span>
                                            <input id="username"  name="j_username" class="form-control" placeholder="Username" type="text" autofocus>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"></span>
                                            <input id="password" name="j_password" class="form-control" placeholder="Password" type="password" value="">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <input type="submit" class="btn btn-lg btn-primary btn-block" value="Войти">
                                    </div>
                                    <c:if test="${not empty param.error}">
                                        <div color="red"> Ошибка входа
                                            : ${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message} </small>
                                            </div>
                                    </c:if>
                                    <%--<c:if test="${not empty error}">--%>
                                        <%--<div class="alert alert-danger" style="width: 285px; margin: 0px auto;" role="alert">--%>
                                                <%--${error}</div>--%>
                                    <%--</c:if>--%>

                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
                <div class="panel-footer ">
                    У вас нет учетной записи! <a href="/signup" onClick="">Зарегистрироваться</a>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>