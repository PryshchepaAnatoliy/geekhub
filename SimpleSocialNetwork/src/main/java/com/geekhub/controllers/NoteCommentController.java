package com.geekhub.controllers;

import com.geekhub.entity.NoteComment;
import com.geekhub.entity.User;
import com.geekhub.entity.UserNote;
import com.geekhub.service.NoteCommentService;
import com.geekhub.service.UserNoteService;
import com.geekhub.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.security.Principal;
import java.util.Date;

@Controller
public class NoteCommentController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserNoteService userNoteService;

    @Autowired
    private NoteCommentService noteCommentService;

    @RequestMapping(value = "/addNoteComment", method = RequestMethod.GET)
    public String addNoteComment(int userId, int noteId, NoteComment noteComment, Principal principal) {
        User user = userService.getUser(userId);
        if(user == null){
            return "helloworld";
        }else {
            int sessionUserId = userService.getIdByUsername(principal.getName());
            Date date = new Date();
            noteComment.setDateOfComment(date);
            noteComment.setUser(userService.getUser(sessionUserId));
            UserNote userNote = userNoteService.getUserNote(noteId);
            noteComment.setUserNote(userNote);
            noteCommentService.addNoteComment(noteComment);
            return "redirect:/profile/"+userId;
        }
    }
}
