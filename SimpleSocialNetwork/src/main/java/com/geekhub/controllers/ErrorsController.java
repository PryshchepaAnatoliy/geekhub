package com.geekhub.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class ErrorsController {

    @RequestMapping(value = "/errors/404.jsp")
    public ModelAndView handle400(Model model) {
        ModelAndView modelAndView = new ModelAndView("viewName");
        modelAndView.addObject("errorCode", "404");
        modelAndView.addObject("message", "Error 404 happens");
        modelAndView.setViewName("hello");
        return modelAndView;

    }
}
