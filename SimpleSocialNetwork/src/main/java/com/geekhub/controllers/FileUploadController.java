package com.geekhub.controllers;

import com.geekhub.entity.Avatar;
import com.geekhub.entity.User;
import com.geekhub.service.AvatarService;
import com.geekhub.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.Principal;

@Controller
public class FileUploadController {

    @Autowired
    private UserService userService;

    @Autowired
    private AvatarService avatarService;

    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public String showUploadForm(HttpServletRequest request) {
        return "/test";
    }


//    headers = "content-type=multipart/*"
    @RequestMapping(value = "/download",method = RequestMethod.POST)
    public String download(
            @ModelAttribute("avatar") Avatar avatar,
            @RequestParam(value="myfile") MultipartFile file,
            Principal principal
    ) {
        User user = userService.findUserByEmail(principal.getName());
        try {
            avatarService.createAvatar(file.getInputStream(), file.getOriginalFilename(),
                    file.getContentType(), user.getId(), file.getSize(), avatar);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "redirect:download";
    }

//    @RequestMapping(value="/signup12", method=RequestMethod.GET)
//    public @ResponseBody String signup1() {
//        return "You can upload a file by posting to this same URL.";
//    }

    @RequestMapping(value = "/signup12", method ={RequestMethod.POST,RequestMethod.GET} )
    public String handleFileUpload(HttpServletRequest request,
                                   @RequestParam CommonsMultipartFile[] fileUpload,String firstName) throws Exception {

        if (fileUpload != null && fileUpload.length > 0) {
            for (CommonsMultipartFile aFile : fileUpload){

                System.out.println("Saving file: " + aFile.getOriginalFilename());

                User uploadFile = new User();
                uploadFile.setFirstName(firstName);
                uploadFile.setPatronymic(firstName);
                uploadFile.setEmail(firstName);
                uploadFile.setPassword(firstName);
//                uploadFile.setFileName(aFile.getOriginalFilename());
                uploadFile.setUserAvatar(aFile.getBytes());
                userService.addUser(uploadFile);
            }
        }

        return "Success";
    }

}