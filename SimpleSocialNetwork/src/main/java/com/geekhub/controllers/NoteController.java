package com.geekhub.controllers;

import com.geekhub.entity.User;
import com.geekhub.entity.UserNote;
import com.geekhub.service.UserNoteService;
import com.geekhub.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.security.Principal;
import java.util.Date;

@Controller
public class NoteController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserNoteService userNoteService;

    @RequestMapping(value = "/addNote", method = RequestMethod.GET)
    public String addNote(Integer userId, UserNote userNote, Principal principal){
        int sessionUserId = userService.getIdByUsername(principal.getName());
        Date date = new Date();
        userNote.setDateOfNote(date);
        userNote.setUserWhoseNote(userService.getUser(sessionUserId));
        userNote.setUser(userService.getUser(userId));
        userNoteService.addUserNote(userNote);
        return "redirect:/profile/"+userId;
//        return "redirec";
    }

    @RequestMapping(value = "/removeNote{noteId}", method = RequestMethod.GET)
    public String removeNote(@PathVariable("noteId") Integer noteId, Principal principal){
        int sessionUserId;
        if (principal != null){
            sessionUserId =  userService.getIdByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
        }else{
            sessionUserId = userService.getIdByUsername(principal.getName());
        }
        userNoteService.removeUserNote(noteId);
        return "redirect:/profile/"+sessionUserId;
    }
}
