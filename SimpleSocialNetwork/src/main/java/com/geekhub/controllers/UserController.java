package com.geekhub.controllers;

import com.geekhub.entity.NoteComment;
import com.geekhub.entity.User;
import com.geekhub.entity.UserNote;
import com.geekhub.service.NoteCommentService;
import com.geekhub.service.UserNoteService;
import com.geekhub.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.security.Principal;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserNoteService userNoteService;

    @RequestMapping(value =  "/welcome**" , method = RequestMethod.GET)
    public ModelAndView welcomePage() {

        ModelAndView model = new ModelAndView();
        model.addObject("title", "Spring Security Hello World");
        model.addObject("message", "This is welcome page!");
        model.setViewName("helloworld");

        return model;
    }

    @RequestMapping(value =  "/page**" , method = RequestMethod.GET)
    public String page() {

        return "page";
    }
    @RequestMapping(value =  "/page1**" , method = RequestMethod.GET)
    public String page1() {

        return "page1";
    }


    @RequestMapping(value="/editUser{id}", method=RequestMethod.GET)
    public ModelAndView updateUser(Map<String, Object> map,@PathVariable Integer id,Principal principal,Model model) {
        User user = userService.getUser(id);
        if(user == null){
            return new ModelAndView("login.jsp");
        }else {
            int sessionUserId  = userService.getIdByUsername(principal.getName());
            map.put("sessionUserId",sessionUserId);
            model.addAttribute("byId", user);
            if (id.equals(userService.getIdByUsername(principal.getName()))) {
                ModelAndView modelAndView = new ModelAndView("profile_settings");
                modelAndView.addObject("user", user);
                return modelAndView;
            } else {
                return new ModelAndView("helloworld");
            }
        }
    }

    @RequestMapping(value="/editUser{id}", method=RequestMethod.POST)
    public ModelAndView editingForm(@ModelAttribute User user, @PathVariable Integer id, Principal principal) {
        if(id.equals(userService.getIdByUsername(principal.getName()))){
            ModelAndView modelAndView = new ModelAndView("redirect:/editUser{id}");
            userService.updateUser(user);
            return modelAndView;
        }else {
            return new ModelAndView("helloworld");
        }
    }

    @RequestMapping("/user_info")
    public String userEditing(Map<String, Object> map,Principal principal) {
        map.put("user", new User());
        map.put("userList", userService.listUser());
        if (principal != null) {
            map.put("username", SecurityContextHolder.getContext().getAuthentication().getName());
        }
        return "user_info";
    }

    @RequestMapping(value = "/profile/{id}", method = RequestMethod.GET)
    public String userProfile1(Map<String, Object> map,Model model, @PathVariable("id") Integer id, Principal principal) {
        User user = userService.getUser(id);
        if(user == null){
            return "login.jsp";
        }else {
            int sessionUserId = sessionUserId = userService.getIdByUsername(principal.getName());
            map.put("sessionUserId",sessionUserId);
            if (principal != null){
                map.put("username",  SecurityContextHolder.getContext().getAuthentication().getName());
            }
            model.addAttribute("byId", user);
            model.addAttribute("userNote", userService.getUserNote(id));
//            model.addAttribute("noteComment", userNoteService.getNoteComment(userNoteService.getUserNote(id).getId()));
            for(UserNote userNote : userService.getUserNote(id)){
                model.addAttribute("noteComment"+userNote.getId(), userNoteService.getNoteComment(userNote.getId()));
//                map.put("id"+userNote.getId(), userNoteService.getNoteComment(userNote.getId()));

                System.out.println("note id "+userNote.getId());
//                System.out.println("getNoteComment "+userNoteService.getNoteComment(userNote.getId()));
            }

//            System.out.println(userNoteService.getNoteComment(userNoteService.getUserNote(id).getId()));
//      map.put("username", principal.getName());
            return "profile";
        }
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public String search(Map<String, Object> map, Principal principal,
                         @RequestParam(value = "firstName",required = false) String firstName,
                         @RequestParam(value = "patronymic",required = false) String patronymic,
                         @RequestParam(value = "city",required = false) String city,
                         @RequestParam(value = "country",required = false) String country,
                         Model model) {
        if(SecurityContextHolder.getContext().getAuthentication().getName().equals("guest")){
            return "protect";
        }
        System.out.println(SecurityContextHolder.getContext().getAuthentication().getName());
        int sessionUserId = userService.getIdByUsername(principal.getName());
            map.put("sessionUserId",sessionUserId);
        if (principal != null){
            map.put("username",  SecurityContextHolder.getContext().getAuthentication().getName());
        }
        return "search";
    }


    @RequestMapping(value ="/search" ,method = RequestMethod.POST)
    public String listUser(Map<String, Object> map,Principal principal,
                           @RequestParam(value = "firstName",required = false) String firstName,
                           @RequestParam(value = "patronymic",required = false) String patronymic,
                           @RequestParam(value = "city",required = false) String city,
                           @RequestParam(value = "country",required = false) String country,
                           Model model) {
        if(SecurityContextHolder.getContext().getAuthentication().getName().equals("guest")){
            return "protect";
        }
        model.addAttribute("find", userService.searching(firstName,patronymic,city,country));
        int sessionUserId = userService.getIdByUsername(principal.getName());
        map.put("sessionUserId",sessionUserId);
        System.out.println(SecurityContextHolder.getContext().getAuthentication().getName());
        if (principal != null){
            map.put("username",  SecurityContextHolder.getContext().getAuthentication().getName());
        }
        return "search";
    }


}
