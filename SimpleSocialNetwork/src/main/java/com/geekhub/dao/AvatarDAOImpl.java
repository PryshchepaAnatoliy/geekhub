package com.geekhub.dao;

import com.geekhub.entity.Avatar;
import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.InputStream;
import java.sql.Blob;

@Repository
public class AvatarDAOImpl implements AvatarDAO {

    @Autowired
    private SessionFactory sessionFactory;
    @Autowired
    private UserDAO userDAO;

    public void createAvatar(InputStream inputStream, String fileName,
                             String contentType, int userId,
                             long fileSize, Avatar avatar){
        Blob blob = Hibernate.getLobCreator(sessionFactory
                .getCurrentSession()).createBlob(inputStream, fileSize);
        avatar.setName(fileName);
        avatar.setUser(userDAO.getUser(userId));
        avatar.setContent(blob);
        avatar.setContentType(contentType);
        sessionFactory.getCurrentSession().save(avatar);
    }
}

