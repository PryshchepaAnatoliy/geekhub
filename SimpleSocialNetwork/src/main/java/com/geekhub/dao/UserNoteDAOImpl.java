package com.geekhub.dao;

import com.geekhub.entity.NoteComment;
import com.geekhub.entity.UserNote;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserNoteDAOImpl implements UserNoteDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public void addUserNote(UserNote userNote){
        sessionFactory.getCurrentSession().save(userNote);
    }

    public void removeUserNote(Integer id) {
        Session currentSession = sessionFactory.getCurrentSession();
        UserNote userNote = (UserNote) currentSession.load(
                UserNote.class, id);
        if (null != userNote) {
//            parentObject.collection.remove(objToDelete);
//            user.followedLocations.remove(userNote);
//            currentSession.update(user);
            currentSession.delete(userNote);
//            sessionFactory.getCurrentSession().delete(userNote);
//            sessionFactory.delete(userNote);
//            sessionFactory.save(userNote);
        }
    }

    @SuppressWarnings("unchecked")
    public List<UserNote> listUserNote() {
        return sessionFactory.getCurrentSession().createQuery("from UserNote").list();
    }

    public List<NoteComment> getNoteComment(Integer id){
        UserNote userNote = (UserNote) sessionFactory.getCurrentSession().get(UserNote.class, id);
        return userNote.getNoteComment();
    }

    public UserNote getUserNote(Integer id) {
        return (UserNote) sessionFactory.getCurrentSession().get(UserNote.class, id);
    }


}
