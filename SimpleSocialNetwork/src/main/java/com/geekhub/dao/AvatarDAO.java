package com.geekhub.dao;

import com.geekhub.entity.Avatar;

import java.io.InputStream;

public interface AvatarDAO {

    public void createAvatar(InputStream inputStream, String fileName,
                             String contentType, int userId,
                             long fileSize, Avatar avatar);
}
