package com.geekhub.service;

import com.geekhub.dao.AvatarDAO;
import com.geekhub.dao.UserDAO;
import com.geekhub.entity.Avatar;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.InputStream;
import java.sql.Blob;

@Service
public class AvatarServiceImpl implements AvatarService {

    @Autowired
    private AvatarDAO avatarDAO;

    @Transactional
    public void createAvatar(InputStream inputStream, String fileName,
                             String contentType, int userId,
                             long fileSize, Avatar avatar) {
            avatarDAO.createAvatar(inputStream,fileName,contentType,userId,fileSize,avatar);
        }
}
