package com.geekhub.service;

import com.geekhub.entity.NoteComment;
import com.geekhub.entity.UserNote;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public interface UserNoteService {
    public void addUserNote(UserNote userNote);
    public void removeUserNote(Integer id);
    public UserNote getUserNote(Integer id);
    public List<UserNote> listUserNote();
    public List<NoteComment> getNoteComment(Integer id);

}
