package com.geekhub.service;


import com.geekhub.dao.NoteCommentDAO;
import com.geekhub.entity.NoteComment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class NoteCommentServiceImpl implements NoteCommentService {
    @Autowired
    private NoteCommentDAO noteCommentDAO;

    @Transactional
    public void addNoteComment(NoteComment noteComment) {
        noteCommentDAO.addNoteComment(noteComment);
    }
    @Transactional
    public NoteComment getNoteComment(Integer id) {
        return noteCommentDAO.getNoteComment(id);
    }
}
