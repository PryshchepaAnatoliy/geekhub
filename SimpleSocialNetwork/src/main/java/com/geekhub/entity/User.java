package com.geekhub.entity;


import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.validator.constraints.Email;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "USER")
public class User {
    @Id
    @Column(name = "ID", unique = true)
    @GeneratedValue
    private Integer id;

//    @Column(name = "USERNAME",unique = true)
//    @Size(min=4, max=20,
//            message="Имя должно быть от 4 до 20 символов")
//    @Pattern(regexp="^[a-zA-Z0-9]+$",
//            message="Имя должно состоять из латинского алфавитаи не иметь пробелов")
//    private String username;

    @Column(name = "EMAIL", unique = true)
    @Pattern(regexp="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+.[A-Za-z]{2,4}",
            message="Не правильно введен email адрес.")
//    @Email
    private String email;

    @Column(name = "PASSWORD")
    @Size(min=6, max=20, message="Пароль должет быть не меньше 6 символов")
    @NotNull(message="Пароль должен быть задан")
    private String password;

    @Column(name ="CONFIRM_PASSWORD")
    private String confirmPassword;

    @Column(name = "ENABLED")
    private Boolean enabled;

    @Column(name = "ROLE")
    private String role;

    @Lob
    @Column(name="USER_AVATAR", nullable=true, columnDefinition="mediumblob")
    private byte[] userAvatar;

    @Column(name = "FIRST_NAME")
    private String firstName;

    @Column(name = "LAST_NAME")
    @NotNull(message="Фамилия должна быть задана")
    @Size(min = 3, message="Длина фамилии должна быть больше трех")
    private String lastName;

    @Column(name = "PATRONYMIC")
    private String patronymic;

    @Column(name = "CITY")
    private String city;

    @Column(name = "COUNTRY")
    private String country;

    @Column(name = "ADDRESS")
    private String address;

    @Column(name = "DATE_OF_BIRTH")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern="dd/MM/yyyy")
    @NotNull
    private Date birthday;

    @Column(name = "PHONE")
    private String phone;

    @Column(name = "GENDER")
    private String gender;

    @OneToMany(mappedBy = "user", fetch=FetchType.EAGER)
    @Fetch(value = FetchMode.SELECT)
    private List<UserNote> userNote;

    @OneToMany(mappedBy = "user",fetch=FetchType.EAGER )
    @Fetch(value = FetchMode.SELECT)
    private List<NoteComment> noteComment;

    @OneToOne(mappedBy = "user")
    private Avatar avatar;



    //    @Column(name = "CONFIRMPASSWORD")
//    @Size(min=6, max=20,
//            message="Пароль должет быть не меньше 6 символов")
//    private String confirmPassword;




    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public byte[] getUserAvatar() {
        return userAvatar;
    }

    public void setUserAvatar(byte[] userAvatar) {
        this.userAvatar = userAvatar;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String secondName) {
        this.lastName = secondName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String surname) {
        this.patronymic = surname;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public List<UserNote> getUserNote() {
        return userNote;
    }

    public void setUserNote(List<UserNote> userNote) {
        this.userNote = userNote;
    }

    public List<NoteComment> getNoteComment() {
        return noteComment;
    }

    public void setNoteComment(List<NoteComment> noteComment) {
        this.noteComment = noteComment;
    }

    public Avatar getAvatar() {
        return avatar;
    }

    public void setAvatar(Avatar avatar) {
        this.avatar = avatar;
    }
}