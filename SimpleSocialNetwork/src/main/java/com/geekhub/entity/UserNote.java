package com.geekhub.entity;


import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;


import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "USER_NOTE")
public class UserNote {
    @Id
    @Column(name = "NOTE_ID")
    @GeneratedValue
    private Integer id;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "DATE_OF_NOTE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateOfNote;

    @ManyToOne(fetch=FetchType.EAGER)
    @Fetch(value = FetchMode.SELECT)
    @JoinColumn(name ="user_id_for_who")
    private User user;

    @ManyToOne(fetch=FetchType.EAGER)
    @Fetch(value = FetchMode.SELECT)
    @JoinColumn(name ="user_id_whose_note")
    private User userWhoseNote;

    @OneToMany(mappedBy = "userNote",cascade = CascadeType.ALL,fetch=FetchType.EAGER)
    @Fetch(value = FetchMode.SELECT)
    private List<NoteComment> noteComment;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDateOfNote() {
        return dateOfNote;
    }

    public void setDateOfNote(Date dateOfNote) {
        this.dateOfNote = dateOfNote;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getUserWhoseNote() {
        return userWhoseNote;
    }

    public void setUserWhoseNote(User userWhoseNote) {
        this.userWhoseNote = userWhoseNote;
    }

    public List<NoteComment> getNoteComment() {
        return noteComment;
    }

    public void setNoteComment(List<NoteComment> noteComment) {
        this.noteComment = noteComment;
    }
}