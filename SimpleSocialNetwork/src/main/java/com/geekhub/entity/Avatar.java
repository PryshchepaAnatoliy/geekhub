package com.geekhub.entity;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.sql.Blob;

@Entity
@Table(name = "USER_AVATAR")
public class Avatar {

    @Id
    @Column(name = "ID", unique = true)
    @GeneratedValue
    private Integer id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "CONTENTTYPE")
    private String contentType;

    @Lob
    @Column(name="CONTENT", nullable=false, columnDefinition="mediumblob")
    private Blob content;

    @OneToOne(fetch=FetchType.LAZY)
//    @Fetch(value = FetchMode.SELECT)
    @JoinColumn(name ="user_id_avatar",nullable = false)
    private User user;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public Blob getContent() {
        return content;
    }

    public void setContent(Blob content) {
        this.content = content;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
