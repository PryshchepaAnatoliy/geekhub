package com.geekhub;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

public class CreateFile extends HttpServlet {

    private static void exists(String fileName, PrintWriter out) throws FileNotFoundException {
        File file = new File(fileName);
        if (!file.exists()){
            out.print("<p><b>File not found!</b></p>");
            out.print("<a href=\"/filemanager\">Main page.</a>");
            throw new FileNotFoundException(file.getName());
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, FileNotFoundException {
        resp.setContentType("text/html;charset=UTF-8");
        PrintWriter out = resp.getWriter();
        String directory = req.getParameter("directory");
        String nameFile = req.getParameter("nameFile");
        String fileDescription=req.getParameter("fileDescription");
        if(directory == "" || nameFile == "" ){
            req.getRequestDispatcher("/filemanager").forward(req, resp);
        }
        String s = directory.substring(0, 4);
        if (!s.equals("G://")){
            String search = "//";
            String sub = "\\\\";
            String result = "";
            int i;
            do {
                i=4;
                if(i != 4) {
                    result = directory.substring(0, i);
                    result = result + sub;
                    result = result + directory.substring(i + search.length());
                    directory = result;
                }
            } while(i != 4);
        }
        if (req.getParameter("create") != null) {
            File folder = new File(directory);
            if (!folder.exists()) {
                folder.mkdir();
                out.print("<p><b>Directory: " + folder.getPath() + " created!</b></p>");
            }
            File file = new File(directory + nameFile);
            if (nameFile.contains(".txt")) {
                try {
                    if (!file.exists()) {
                        file.createNewFile();
                    }
                    PrintWriter writer = new PrintWriter(file.getAbsoluteFile());
                    try {
                        writer.print(fileDescription);
                    } finally {
                        writer.close();
                    }
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }else {
                if (!file.exists()) {
                    file.createNewFile();
                }
            }
                out.print("<p><b>File: " + file.getPath() + " created!</b></p>");
                out.print("<a href=\"/filemanager\">Main page.</a>");
        } else if (req.getParameter("delete") != null) {
            File file = new File(directory + nameFile);
            exists(directory + nameFile, out);
            if (!file.exists()) {
                System.out.println("File: " + file.getPath() + "not found! \n");
                out.print("<a href=\"/filemanager\">Main page.</a>");
            }else {
                file.delete();
            }
        } else if (req.getParameter("read") != null) {
            if (nameFile.contains(".txt")) {
                StringBuilder sb = new StringBuilder();
                exists(directory + nameFile, out);
                try {
                    BufferedReader in = new BufferedReader(new FileReader(directory + nameFile));
                    try {
                        String a;
                        while ((a = in.readLine()) != null) {
                            sb.append(a);
                            sb.append("\n");
                        }
                    } finally {
                        in.close();
                    }
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
                out.print("<html>");
                out.print("<p><b>You want read: " + directory + nameFile + "?</b></p>");
                out.print("<form id=\"data\" action=\"/filemanager\" method=\"GET\">");
                out.print("YES!");
                out.print("<input type=\"radio\" name=\"disoffile\" value=\"" + sb.toString() + "\" checked=\"checked\">");
                out.print("NO!");
                out.print("<input type=\"radio\" name=\"disoffile\" value=\"\">");
                out.print("<input type =\"submit\" value =\"Acsses\">");
                out.print("</form>");
                out.print("</html>");
            }else {
                out.print("<html>");
                out.print("<head>");
                out.print("<title>Error</title>");
                out.print("</head>");
                out.print("The file is not readable. I can not read it...");
                out.print("Please enter file for reading!");
                out.print("<br>");
                out.print("<a href=\"/filemanager\">Main page.</a>");
                out.print("</html>");
            }
        }
    }
}
