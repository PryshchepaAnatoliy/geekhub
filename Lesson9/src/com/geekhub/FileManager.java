package com.geekhub;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

public class FileManager extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=UTF-8");
        String dir = "G:\\";
        String currentFolder;
        String disOfFile;
        if ((disOfFile = req.getParameter("disoffile")) == null) {
            disOfFile = "";
        }
        System.out.println(disOfFile);
        if ((currentFolder = req.getParameter("folder")) == null) {
            currentFolder = dir;
        }

        PrintWriter out = resp.getWriter();
        File[] allFiles = new File(currentFolder).listFiles();

        out.print("<html>");
        out.print("<head>");
        out.print("<title>Manager</title>");
        out.print("<meta http-equiv=\"Content-Type\" content=\"text/html\" charset=utf-8\">");
        out.print("</head>");
        out.print("<body>");
        out.print("<form action=\"createFile\" method=\"GET\">");
        out.print("<p><b>Description for file:</b></p>");
        out.print("<textarea rows=\"10\" cols=\"45\" type=\"text\" name=\"fileDescription\" >" + disOfFile + "</textarea>");
        out.print("<br>");
        out.print("Directory for file: <input type=\"text\" required  name=\"directory\" value = \"" + currentFolder + "\\\">");
        out.print("File Name: <input type=\"text\" required name=\"nameFile\">");
        out.print("<br>");
        out.print("<input type=\"submit\" name=\"create\" value=\"Create/Change\"> ");
        out.print("<input type=\"submit\" name=\"delete\" value=\"Delete\"> ");
        out.print("<input type=\"submit\" name=\"read\" value=\"Read\"> ");
        out.print("</form>");


        out.write("<div>");
        for (int i = 0; i < allFiles.length; i++) {
            if (allFiles[i].isDirectory()) {
                out.write("<a href=\"/filemanager?folder=" + currentFolder + "\\" + allFiles[i].getName() + "\">" + currentFolder + "\\" + allFiles[i].getName() + "</a>");
                out.write("<br>");
            }
            if (allFiles[i].isFile()) {
                out.write("<div>" + currentFolder + "\\" + allFiles[i].getName() + "</div>");
            }
        }
        out.write("</div>");
        out.print("</body>");
        out.print("</html>");
    }
}
