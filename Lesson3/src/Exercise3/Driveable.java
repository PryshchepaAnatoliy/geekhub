package Exercise3;

import Exercise3.Exception.*;

/**
 * Created by Анатолий on 25.10.2014.
 */
public interface Driveable {

    public void accelerate() throws BigSpeedException,SmallSpeedException;
    public void brake();
    public void turn();
    public void starting() throws BigSpeedException,SmallSpeedException;
}
