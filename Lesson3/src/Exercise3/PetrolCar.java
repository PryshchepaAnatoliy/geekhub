package Exercise3;

import Exercise3.Exception.*;


import java.util.Scanner;

public class PetrolCar extends Vehicle {
    GasEngine gasEngine = new GasEngine();
    Wheel wheel = new Wheel();
    GasTank tank = new GasTank(60,20);
    public int maxSpeed=180;
    public double startFuel= tank.refuel();
    public double currentFuel;
    public double distance = 0;
    public double residueFuel = currentFuel;
    public int engineSpeed;
    private int rotationWheels;

    public double getCurrentFuel() {
        return startFuel;
    }

    public void setCurrentFuel(double startFuel) {
        this.startFuel = startFuel;
    }

    public int getEngineSpeed() {
        return engineSpeed;
    }

    public void setEngineSpeed(int engineSpeed) {
        this.engineSpeed = engineSpeed;
    }

    public void setRotationWheels(int rotationWheels) {
        this.rotationWheels = rotationWheels;
    }

    public int getRotationWheels() {
        return rotationWheels;
    }


    public void distance(){
        if (currentFuel<=0){
            distance = this.distance + getCurrentFuel() * 15;
            currentFuel = 0;
            brake();
            System.out.println("Закончился бензин!");
        }else {
            residueFuel = (int) (getEngineSpeed() * 0.01);
            distance = this.distance + residueFuel * 15;
        }
    }

    public void setStatusOfCar(int speed, int engineSpeed, int rotationWheels, Course course){
        setSpeed(speed);
        setEngineSpeed(engineSpeed);
        setRotationWheels(rotationWheels);
        setCourse(course);

    }
    public void getStatusOfCar(){
        System.out.println("Текущие показатели автомобиля:");
        System.out.println("Скорость -" + getSpeed() + "км/час");
        System.out.println("Обороты двигателя -"+ getEngineSpeed());
        System.out.println("Обороты колес -" + getRotationWheels());
        System.out.println("Курс -:" + getCourse());
        System.out.println("Текущие состояние бака -" + currentFuel + "л.");
        System.out.println("Пройденная дистанция -" + distance + "км");
    }

    @Override
    public void starting() throws BigSpeedException,SmallSpeedException {
        turn();
        int speed;
        setCurrentFuel(0);
        System.out.println("С какой скоростью вы хотите двигатся?\n" +
                           "Не забывайте что максимальная скорость вашего автомобиля 180км/час.Рекомендуемая 60км/час");
        Scanner sc = new Scanner(System.in);
        if (sc.hasNextInt()) {
            speed = sc.nextInt();
            if (speed > maxSpeed) {
                throw  new BigSpeedException(maxSpeed);
            }
            if (speed <= 0){
                throw  new SmallSpeedException();
            }else {
                setStatusOfCar(speed, gasEngine.createForce(speed), wheel.usingForce(gasEngine.createForce(speed)), getCourse());
                currentFuel -= (int) (getEngineSpeed() * 0.01);
                setCurrentFuel(currentFuel);
                distance();
                getStatusOfCar();
                System.out.println("Сейчас ваша скорость - " + speed + "км/час");
            }
        }else {
            speed=60;
            setStatusOfCar(speed, gasEngine.createForce(speed), wheel.usingForce(gasEngine.createForce(speed)), getCourse());
            currentFuel -= (int) (getEngineSpeed() * 0.01);
            setCurrentFuel(currentFuel);
            distance();
            getStatusOfCar();
            System.out.println("Вы ввели не верное число или это вовсе не число.");
        }
        accelerate();
    }

    @Override
    public void accelerate() throws BigSpeedException,SmallSpeedException {
        int acc;
        turn();
        System.out.println("Ускорение!\nК какой скорости вы хотите ускорится или понизится?");
        Scanner sc = new Scanner(System.in);
        if (sc.hasNextInt()) {
            acc = sc.nextInt();
            if (acc > maxSpeed) {
                throw  new BigSpeedException(maxSpeed);
            }
            if (acc < 0) {
                throw  new SmallSpeedException();
            }
            if (acc == 0) {
                setStatusOfCar(0, 0, 0, getCourse());
                currentFuel -= getEngineSpeed() * 0.01;
                distance();
                getStatusOfCar();
            }else{
                setStatusOfCar(acc, gasEngine.createForce(acc), wheel.usingForce(gasEngine.createForce(acc)), getCourse());
                currentFuel -= getEngineSpeed() * 0.01;
                distance();
                getStatusOfCar();
                brake();
            }
        }else {
            acc = 60;
            System.out.println("Вы ввели не верное число или это вовсе не число.\n" +
                               "По этому я установил рекомендуемую скорость " + acc + "км/час");
            setStatusOfCar(acc, gasEngine.createForce(acc), wheel.usingForce(gasEngine.createForce(acc)), getCourse());
            currentFuel = this.currentFuel - (int) (getEngineSpeed() * 0.01);
            distance();
            getStatusOfCar();
            brake();
        }
    }

    @Override
    public void turn() {
        int s;
        System.out.println("Введите курс куда вы хотите поехать:");
        System.out.println("1-Север");
        System.out.println("2-Юг");
        System.out.println("3-Запад");
        System.out.println("4-Восток");
        Scanner sc = new Scanner(System.in);
        if(sc.hasNextInt()){
            s = sc.nextInt();
            switch (s) {
                case 1:
                    setCourse(Course.NORTH);
                    break;
                case 2:
                    setCourse(Course.SOUTH);
                    break;
                case 3:
                    setCourse(Course.WEST);
                    break;
                case 4:
                    setCourse(Course.EAST);
                    break;
                default:
                    setCourse(Course.NORTH);
                    System.out.println("Вы ввели не верное число, по этому я посмел поставить курс на север");
                    break;
            }
        }else{
            setCourse(Course.NORTH);
            System.out.println("Вы ввели не верное число, по этому я посмел поставить курс на север");
        }
    }

    @Override
    public void brake() {
        int s;
        if((getSpeed() == 0)||(currentFuel <= 0)){
            setStatusOfCar(0, 0, 0, getCourse());
        }else{
            System.out.println("Остановить автомобиль?\n1-Да \n2-Нет");
            Scanner sc = new Scanner(System.in);
            if(sc.hasNextInt()) {
                s = sc.nextInt();
                switch (s) {
                    case 1:
                        System.out.println("Машина стоит!");
                        setStatusOfCar(0, 0, 0, getCourse());
                        getStatusOfCar();
                        break;
                    case 2:
                        System.out.println("Вы продолжаете двежение!");
                        try {
                            accelerate();
                        } catch (BigSpeedException e) {
                            System.out.println(e.getMessage()+e.getSpeed());
                        }catch (SmallSpeedException e) {
                            System.out.println(e.getMessage());
                        }
                        getStatusOfCar();
                        break;
                    default:
                        System.out.println("Машина стоит!");
                        setStatusOfCar(0, 0, 0, getCourse());
                        getStatusOfCar();
                        break;
                }
            }
        }
    }
}
