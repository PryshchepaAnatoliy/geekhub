package Exercise3;

/**
 * Created by Анатолий on 28.10.2014.
 */
public interface ForceProvider {
    public int createForce(int speed);
}
