package Exercise3.Exception;

public class BigSpeedException extends Exception{

    private int speed;
    public BigSpeedException(){
        this(0);
    }
    public BigSpeedException(int speed){
        this.speed = speed;
    }
    public int getSpeed(){
        return speed;
    }
    @Override
    public String getMessage() {
        return "Ошибка: Ваш транспорт не может ехать свыше(км/час) - ";
    }
}
