package Exercise3.Exception;

public class InvalidValueException extends Exception {
    @Override
    public String getMessage() {
        return "Вы ввели не верное число или это вовсе не число.";
    }
}