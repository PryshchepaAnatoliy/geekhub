package Exercise3.Exception;

/**
 * Created by Анатолий on 10.11.2014.
 */
public class SmallSpeedException extends Exception {
    @Override
    public String getMessage() {
        return "Ошибка: Ваш транспорт не может ехать 0 и ниже км/час!";
    }
}
