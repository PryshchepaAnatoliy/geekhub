package Exercise3;

import java.util.Scanner;

public class SolarBattery implements EnergyProvider {
    private final int maxEnergy;
    private float Energy;

    SolarBattery(int maxE, float e){
        this.maxEnergy = maxE;
        this.Energy = e;
    }

    public double refuel() {
        System.out.println("Чтобы двигатся вам нужно зарядить аккумулятор, хотя он уже заряжан на "+ Energy +" Дж." +
                           "\nНужно ли дозарядить?\n1 - Да\n2 - Нет ");
        Scanner sc = new Scanner(System.in);
        if (sc.hasNextInt()) {
            int s = sc.nextInt();
            switch (s) {
                case 1:
                    int filler;
                    System.out.println("Сейчас аккумулятор заряжан на - " + Energy + " Дж.");
                    System.out.println("Одина минута зарядки становит - " + 120 + " Дж.");
                    System.out.println("На сколько вы хотите дозарядить аккумулятор?" +
                                       "\nНе забывайте что максимальный заряд аккумулятора " + maxEnergy + " Дж.");
                    if (sc.hasNextInt()) {
                        filler = sc.nextInt();
                        float mn = filler/120;
                        if (filler < 0) {
                            System.out.println("Вы ввели не верное количество!" +
                                               "\nПо этому вы не дозарядили аккумулятор, Сейчас аккумулятор заряжан на - " + Energy + " Дж.");
                            return Energy;
                        }
                        float fillerNorm = filler - Energy;
                        Energy += filler;
                        if (Energy > maxEnergy) {
                            Energy = maxEnergy;
                            mn = fillerNorm/120;
                            System.out.println("Я вижу вы забыли что максимальное содержание заряда аккумулятора - " + maxEnergy + " Дж." +
                                               "\nПо этому аккумулятор был заряжан до - " + maxEnergy + " Дж." +
                                               "\nЧто бы зарядиться вы вынуждены подождать - " + mn + " мн." +
                                               "\nСейчас аккумулятор заряжан на - " + Energy + " Дж.");
                        } else {
                            System.out.println("Вы зарядили аккумулятор на " + filler + " Дж." +
                                    "\nЧто бы зарядиться вы вынуждены подождать - " + mn + " мн." +
                                    "\nСейчас аккумулятор заряжан на - " + Energy + " Дж.");
                        }
                    } else {
                        System.out.print("Вы ввели не верное количество.\nСейчас аккумулятор заряжан на - " + Energy + " Дж.");
                    }
                    return Energy;
                case 2:
                    System.out.println("Сейчас аккумулятор заряжан на - " + Energy + " Дж.");
                    return Energy;
                default:
                    System.out.println("Вы ввели не верное число.");
                    return Energy;
            }
        } else {
            System.out.println("Вы ввели не верное число или это вовсе не число.");
        }
        return Energy;
    }
}
