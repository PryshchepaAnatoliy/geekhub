package Exercise3;

import Exercise3.Exception.*;

abstract class Vehicle implements Driveable {

    private int speed;
    private Course course;

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getSpeed() {
        return speed;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public Course getCourse() {
        return course;
    }

    public abstract void accelerate() throws BigSpeedException,SmallSpeedException;
    public abstract void brake();
    public abstract void turn();
    public abstract void starting() throws BigSpeedException,SmallSpeedException;

}
