package Exercise3;

import Exercise3.Exception.*;

import java.util.Scanner;

public class Main {
    public static void main(String []args) throws InvalidValueException {
        Driveable vehicle;
        int s;
        Scanner sc = new Scanner(System.in);
        System.out.println("На чем вы хотите путишествовать?");
        System.out.println("1 - Автомобиль на бензине");
        System.out.println("2 - Автомобиль на солничной енергии");
        System.out.println("3 - Моторная лодка");
        if(sc.hasNextInt()){
            s = sc.nextInt();
            switch (s) {
                case 1:
                    try {
                        vehicle =  new PetrolCar();
                        vehicle.starting();
                    } catch (BigSpeedException e) {
                        System.out.println(e.getMessage()+e.getSpeed());
                    } catch (SmallSpeedException e) {
                        System.out.println(e.getMessage());
                    }
                    break;
                case 2:
                    try {
                        vehicle = new SolarPoweredCar();
                        vehicle.starting();
                    } catch (BigSpeedException e) {
                        System.out.println(e.getMessage()+e.getSpeed());
                    } catch (SmallSpeedException e) {
                        System.out.println(e.getMessage());
                    }

                    break;
                case 3:

                    try {
                        vehicle = new Boat();
                        vehicle.starting();
                    } catch (BigSpeedException e) {
                        System.out.println(e.getMessage()+e.getSpeed());
                    } catch (SmallSpeedException e) {
                        System.out.println(e.getMessage());
                    }
                    break;
                default:
                    throw new InvalidValueException();
            }
        }else{
            throw new InvalidValueException();
        }
    }
}
