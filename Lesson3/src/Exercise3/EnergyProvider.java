package Exercise3;

/**
 * Created by Анатолий on 28.10.2014.
 */
public interface EnergyProvider {
    public double refuel();
}
