package Exercise3;

public class Wheel implements ForceAcceptor {

    ElectricEngine electricEngine= new ElectricEngine();

    @Override
        public int usingForce(int engineSpeed) {
            int rotationWheels = electricEngine.createForce(engineSpeed)*10;
            return rotationWheels;
    }
}
