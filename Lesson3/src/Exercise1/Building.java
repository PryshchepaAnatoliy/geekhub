package Exercise1;


public class Building implements Comparable<Building>{

    String street;
    int number;

    Building(String street, int number){
        this.street=street;
        this.number=number;
    }

    public void output() {
        StringBuffer s = new StringBuffer();
        s.append(street).append("-").append(number).append("\n").append("-----------");
        System.out.println(s.toString());
    }

    @Override
    public int compareTo(Building obj) {
        Building entry = obj;
        if (entry.number != this.number) {
            if (this.number < entry.number) {
                return -1;
            } else {
                return 1;
            }
        }
        return 0;
    }

//    public int compareTo(Building obj) {
//        Building entry = obj;
//        int result = street.compareTo(entry.street);
//        if(result != 0) {
//            return result;
//        }
//        result = number - entry.number;
//        if(result != 0) {
//            return result / Math.abs( result );
//        }
//        return 0;
//    }
}
