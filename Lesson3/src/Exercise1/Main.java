package Exercise1;

import java.util.Scanner;

public class Main {
    public static int s;

    public static Comparable[] sort(Comparable[] array) {
        array = array.clone();
     //  Arrays.sort(array);
        bubbleSort((Building[]) array);
        return array;
    }
    public static Building[] bubbleSort(Building[] arr) {
        for (int i = arr.length - 1; i >= 0; i--) {
            for (int j = 0; j < i; j++) {
                if (arr[j].compareTo(arr[j + 1])== -1){
                    Building a = arr[j];
                    arr[j]=arr[j+1];
                    arr[j + 1]=a;
                }
            }
        }return arr;
    }

    public static void main(String []args){
        System.out.println("Сколько вы хотите создать обьектов?");
        Scanner sc = new Scanner(System.in);
        if (sc.hasNextInt()) {
            s = sc.nextInt();
            if (s < 0) {
                System.out.println("Вы ввели не верный число!" +
                                   "\nПо этому Я создал 10 обьектов.");
                s=10;
            }
        }else {
            System.out.println("Вы ввели не верное число или это вовсе не число."+
                               "\nПо этому Я создал 10 обьектов.");
            s=10;
        }
        Building buildings[] = new Building[s];
        String randStreet[] = {"Шевченко", "Гоголя", "Героям Дніпра", "України"};
        for(int i=0; i<s; i++){
            int randomStreet = (int)(Math.random()*50) + 1;
            int rand = (int)Math.floor(Math.random() * randStreet.length);
            buildings[i] = new Building(randStreet[rand],randomStreet);
        }

        Building[] sortBuilding = (Building[]) sort(buildings);

        System.out.println("________Not sorted_________");
        for (Building o : buildings) {
            o.output();
        }
        System.out.println("________Sorted_________");
        for (Building o : sortBuilding) {
            o.output();
        }
    }
}
