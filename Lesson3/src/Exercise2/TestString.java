package Exercise2;

import java.util.concurrent.TimeUnit;

/**
 * Created by Анатолий on 09.11.2014.
 */
public class TestString {

    public static void testingString(){
        double start = System.currentTimeMillis();
        String s = "Testing String";
        for(int i =0;i<10000;i++){
            s +="Testing String";
        }
        double end = System.currentTimeMillis();
        System.out.println("String testing: length -"+ s.length() +" time -"+(end - start)+"s");
    }
    public static void testingStringBuffer(){
        long start = System.currentTimeMillis();
        StringBuffer s = new StringBuffer("Testing String");
        for(int i =0;i<10000;i++){
            s.append("Testing String");
        }
        long end = System.currentTimeMillis();
        System.out.println("StringBuffer testing: length -"+ s.length() +" time -"+(end - start)+"ms");
    }
    public static void testingStringBuilder(){
        long start = System.currentTimeMillis();
        StringBuilder s = new StringBuilder("Testing String");
        for(int i =0;i<10000;i++){
            s.append("Testing String");
        }
        long end = System.currentTimeMillis();
        System.out.println("StringBuilder testing: length - "+ s.length() +" time - "+ (end - start)+"ms");
    }
    public static void main(String []args) {
        for(int i=0; i<5; i++){
            testingString();
            testingStringBuffer();
            testingStringBuilder();
            System.out.println("--------------------");
        }
    }
}
