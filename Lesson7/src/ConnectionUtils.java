import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Utils class that contains useful method to interact with URLConnection
 */
public class ConnectionUtils {

    /**
     * Downloads content for specified URL and returns it as a byte array.
     * Should be used for small files only. Don't use it to download big files it's dangerous.
     * @param url
     * @return
     * @throws java.io.IOException
     */
    public static byte[] getData(URL url) throws IOException {
        //URLConnection connection = url.openConnection();
        InputStream stream = null;
        ByteArrayOutputStream byteArray = new ByteArrayOutputStream();
        byte[] byteChunk;
        try{
            stream = url.openStream();
            int nRead;
            byteChunk = new byte[16384];
            while ((nRead = stream.read(byteChunk, 0, byteChunk.length)) != -1) {
                byteArray.write(byteChunk, 0, nRead);
            }
            while ((nRead = stream.read(byteChunk)) > 0) {
                byteArray.write(byteChunk, 0, nRead);
            }
        }catch (IOException e){
            System.err.printf("Failed while reading bytes from %s: %s", url.toExternalForm(), e.getMessage());
            e.printStackTrace();
        }finally {
            byteArray.flush();
            if (byteArray != null)byteArray.close();
            if (stream != null)stream.close();
        }
        return byteArray.toByteArray();
    }
}
