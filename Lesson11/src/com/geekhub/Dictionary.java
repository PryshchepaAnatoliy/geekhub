package com.geekhub;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.*;

@Component
public class Dictionary {

    @Autowired
    private ResourceLoader resourceLoader;

    public String engRusDictionary(){
        String text = "";
        try {
            File file = resourceLoader.loadSource("G:\\\\eng-rus.txt");
            BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF8"));
            String s;
            try {
                while ((s = in.readLine()) != null) {
                    text = text + s + "\n";
                }
            } catch (FileNotFoundException e) {
                System.out.println(e);
            } catch (IOException e) {
                System.err.println("Exception occurred:" + e.toString());
                e.printStackTrace();
            } finally {
                in.close();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
       // System.out.println("Dictionary:\n"+text);
        return text;
    }

    public String engUaDictionary(){
        String text = "";
        try {
            File file = resourceLoader.loadSource("G:\\\\eng-ua.txt");
            BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF8"));
            String s;
            try {
                while ((s = in.readLine()) != null) {
                    text = text + s + "\n";
                }
            } catch (FileNotFoundException e) {
                System.out.println(e);
            } catch (IOException e) {
                System.err.println("Exception occurred:" + e.toString());
                e.printStackTrace();
            } finally {
                in.close();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
     //   System.out.println("Dictionary:\n"+text);
        return text;
    }

    public String rusEngDictionary(){
        String text = "";
        try {
            File file = resourceLoader.loadSource("G:\\\\rus-eng.txt");
            BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF8"));
            String s;
            try {
                while ((s = in.readLine()) != null) {
                    text = text + s + "\n";
                }
            } catch (FileNotFoundException e) {
                System.out.println(e);
            } catch (IOException e) {
                System.err.println("Exception occurred:" + e.toString());
                e.printStackTrace();
            } finally {
                in.close();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
     //   System.out.println("Dictionary-"+text);
        return text;
    }

    public String uaEngDictionary(){
        String text = "";
        try {
            File file = resourceLoader.loadSource("G:\\\\ua-eng.txt");
            BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF8"));
            String s;
            try {
                while ((s = in.readLine()) != null) {
                    text = text + s + "\n";
                }
            } catch (FileNotFoundException e) {
                System.out.println(e);
            } catch (IOException e) {
                System.err.println("Exception occurred:" + e.toString());
                e.printStackTrace();
            } finally {
                in.close();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Dictionary-"+text);
        return text;
    }

}
