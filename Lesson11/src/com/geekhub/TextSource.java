package com.geekhub;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.*;

@Component
public class TextSource {

    @Autowired private ResourceLoader resourceLoader;
    public String pathOfFile;

    public String fullText(){
        String text = new String();
        try {
            File file = resourceLoader.loadSource(resourceLoader.loadPath());
            BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF8"));
            String s;
            try {
                while ((s = in.readLine()) != null) {
                    text = text + s + "\n";
                }
                pathOfFile = file.getPath();
            } catch (FileNotFoundException e) {
                System.out.println(e);
            } catch (IOException e) {
                System.err.println("Exception occurred:" + e.toString());
                e.printStackTrace();
            } finally {
                in.close();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return text;
    }
}
