package com.geekhub;

import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

@Component
public class ResourceLoader {
    public String loadPath(){
        String directory=null;
        File file = null;
        boolean i = true;
        while (!i == false) {
            System.out.println("Enter the path to the file.");
            Scanner sc = new Scanner(System.in);
            directory = sc.next();
            file = new File(directory);
            if (file.exists() && file.isFile()) {
                i = false;
                System.out.println("Path correct!");
            } else {
                System.out.println("Path wrong!");
            }
        }
        return directory;
    }


    public File loadSource(String path) throws IOException {
        File file = new File(path);
        return file;
    }
}
