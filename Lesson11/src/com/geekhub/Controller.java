package com.geekhub;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by Анатолий on 14.02.2015.
 */
public class Controller {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        Translator translator = context.getBean(Translator.class);
        translator.translate();
    }
}
