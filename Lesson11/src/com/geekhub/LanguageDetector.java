package com.geekhub;

import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.*;

@Component
public class LanguageDetector {

    @Autowired private ResourceLoader resourceLoader;
    @Autowired private TextSource textSource;

    private  String[] engLetters = { "41", "42", "43", "44", "45", "46", "47", "48", "49",
            "4a", "4b", "4c", "4d", "4e", "4f", "50", "51", "52", "53", "54", "55", "56", "57", "58",
            "59", "5a","61", "62", "63", "64", "65", "66", "67", "68", "69", "6a", "6b", "6c", "6d",
            "6e", "6f", "70", "71", "72","73", "74", "75", "76", "77", "78", "79", "7a"};
    private  String[] rusLetters = { "410", "411", "412", "413", "414",
            "415", "401", "416", "417", "418", "41a", "41b", "41c", "41d", "41e",
            "41f", "420", "421", "422", "423", "424", "425", "426", "427", "428",
            "429", "42e", "42f", "42d", "42b", "42a" ,"430", "431", "432", "433",
            "434", "435", "451", "436", "437", "438", "43a", "43b", "43c", "43d",
            "43e", "43f", "440", "441", "442", "443", "444", "445", "426", "447",
            "448", "449", "44e", "44f", "44d", "44b", "44a" };
    private  String[] ukrLetters = { "410", "411", "412", "413", "414", "415",
            "404", "416", "417", "418", "406", "407", "419", "41a", "41b", "41c",
            "41d", "41e", "41f", "420", "421", "422", "423", "424", "425", "426",
            "427", "428", "429", "42e", "42f", "42c", "430", "431", "432", "433", "434", "435",
            "454", "436", "437", "438", "456", "457", "439", "43a", "43b", "43c",
            "43d", "43e", "43f", "440", "441", "442", "443", "444", "445", "446",
            "447", "448", "449", "44e", "44f", "44c" };
    public String[][] letters = {engLetters, rusLetters, ukrLetters };

    public String foundLanguages(String[] stringArray) {
        String language = null;
        if (stringArray == engLetters){
            return language = "English";
        }
        else if (stringArray == rusLetters) {
            return language = "Russian";
        }
        else {
            return language = "Ukrainian";
        }
    }

    public String identify(String string) {
        int rus=0;
        int eng=0;
        int ua=0;
        char[] charArray = string.toCharArray();
        String[] str = new String[charArray.length];
        for (int i = 0; i < charArray.length; i++) {
            int codePoint = Character.codePointAt(charArray, i);
            str[i] = Integer.toHexString(codePoint);
            for (String[] lettersArray : letters)
                for (String foundLetter : lettersArray)
                    if (str[i].equals(foundLetter)) {
                        String s = foundLanguages(lettersArray);
                        if (s.equals("Ukrainian")){
                            ua++;
                        }else  if(s.equals("Russian")){
                            rus++;
                        }else
                            eng++;
                    }
        }
        String lan ;
        if (rus > eng && rus > ua){
            lan = "Russian";
            return lan;
        }else if(eng >= ua && eng > rus){
            lan = "English";
            return lan;
        }else{
            lan = "Ukrainian";
            return lan;
        }
    }
    public  String language(){
        String language = "English";
        try {
            File file = resourceLoader.loadSource(textSource.pathOfFile);
            BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(file),"UTF8"));
            String s;
            String s2 = "";
            try {
                while ((s = in.readLine()) != null) {
                    s2=s2+s+"\n";
                }
            } catch(FileNotFoundException e) {
                System.out.println(e);
            } catch (IOException e){
                System.err.println("Exception occurred:" + e.toString());
                e.printStackTrace();
            } finally {
                in.close();
            }
            String [] a = {s2};
            String string = a[0];
            language = identify(string);
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            System.out.println("Language of text -"+language);
            return language;
       }
    }
}
