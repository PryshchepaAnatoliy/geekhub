package com.geekhub;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Scanner;

@Component
public class Translator {

    @Autowired private TextSource textSource;
    @Autowired private Dictionary dictionary;
    @Autowired private LanguageDetector languageDetector;

    private String whatLanguage(){
        String whatLanguage = null;
        boolean i = true;
        while (!i == false) {
            System.out.println("On which language you want to translate");
            Scanner sc = new Scanner(System.in);
            whatLanguage = sc.next();
            if (whatLanguage.equals("English") || whatLanguage.equals("english")) {
                i = false;
            } else if (whatLanguage.equals("Russian") || whatLanguage.equals("russian")) {
                i = false;
            }else if(whatLanguage.equals("Ukrainian") || whatLanguage.equals("ukrainian")){
                i = false;
            }else{
                System.out.println("Incorrect language!");
                System.out.println("It must be English or Russian or Ukrainian");
            }
        }
        return whatLanguage;
    }

    private String setDictionary(String languageOfText, String whatLanguage){
        if (languageOfText.equals("English") && whatLanguage.equals("Ukrainian") || whatLanguage.equals("ukrainian")){
            return dictionary.engUaDictionary();
        }else if(languageOfText.equals("Russian")&& whatLanguage.equals("English") || whatLanguage.equals("english")){
            return dictionary.rusEngDictionary();
        }else if(languageOfText.equals("English")&& whatLanguage.equals("Russian") || whatLanguage.equals("russian")) {
            return dictionary.engRusDictionary();
        }else if(languageOfText.equals("Ukrainian")&& whatLanguage.equals("English") || whatLanguage.equals("english")){
                return dictionary.uaEngDictionary();
        }else
            return dictionary.uaEngDictionary();
    }


    public void translate(){
        String originalText = textSource.fullText();
        String translateText = originalText;
        String languageOfText = languageDetector.language();
        String whatLanguage = whatLanguage();
        String dictionary = setDictionary(languageOfText,whatLanguage);
        String[] splitText = originalText.split("[ \\n]");
        String[] splitDictionary = dictionary.split("[=\\n]");
        for(String a1 : splitText){
            System.out.println(a1);
            for (int i =0; i<splitDictionary.length;i++){
                    if (splitDictionary[i].equals(a1)){
                        translateText = translateText.replaceAll(a1, splitDictionary[i + 1]);
                    }
               }
            }
        System.out.println("Original:\n"+originalText);
        System.out.println("Translate:\n"+translateText);
    }
}
