package com.geekhub.jstl;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

public class PageNavigationTag extends TagSupport {

    private int resultsPerPage;
    private int totalResults;
    private int currentPage;

    public PageNavigationTag() {
    }

    public void setResultsPerPage(int resultsPerPage) {
        this.resultsPerPage = resultsPerPage;
    }

    public void setTotalResults(int totalResults) {
        this.totalResults = totalResults;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int doStartTag() throws JspException {
        System.out.println("Количество елементов на странице:" + resultsPerPage);
        System.out.println("Общие количество:" + totalResults);
        System.out.println("текущая страница:" + currentPage);

        JspWriter out = pageContext.getOut();
        double allPages = Math.ceil((double)totalResults / resultsPerPage);
        int firsElement = (resultsPerPage*(currentPage-1)+1);
        int lastElement = (resultsPerPage*(currentPage-1)+resultsPerPage);
        if(lastElement>=totalResults){
            lastElement = totalResults;
        }
        try {
            out.println("<div class=\"block1\">");
            out.println("<div align=\"center\">");
            out.println(firsElement+"-"+lastElement+"/"+totalResults);
            out.println("</div>");
            out.println("<table border=\"0\" cellpadding=\"10\" cellspacing=\"1\">");
            out.println("<tr>");
            if(currentPage != 1) {
                out.println("<th><a href=\"/index.jsp?page="+ (currentPage - 1) +"&perPage="+resultsPerPage+"\">Previous</a>");
                out.println("</th>");
            }
            for(int i=1; i<=allPages; i++){
                if (i==currentPage){
                    out.println("<th bgcolor= \"blue\"><a href=\"/index.jsp?page="+ i +"&perPage="+resultsPerPage+"\">"+i+"</a>");
                    out.println("</th>");
                }else{
                    out.println("<th><a href=\"/index.jsp?page="+ i +"&perPage="+resultsPerPage+"\">"+i+"</a>");
                    out.println("</th>");
                }
            }
            if(currentPage!= allPages) {
                out.println("<th><a href=\"/index.jsp?page="+ (currentPage + 1) +"&perPage="+resultsPerPage+"\">Next</a>");
                out.println("</th>");
            }
            out.println("</tr>");
            out.println("</table>");
            out.println("</div>");
        } catch (Exception e) {
            System.out.println(e);
        }
//
        return EVAL_BODY_INCLUDE;
    }
}