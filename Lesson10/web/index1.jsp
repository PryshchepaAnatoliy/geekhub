<%@ page import="java.util.ArrayList" %>
<%@ page language="java" contentType="text/html; charset=US-ASCII"
         pageEncoding="US-ASCII"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
  <title>Custom Tag Example</title>
  <%@ taglib uri="/WEB-INF/PageNavigationTag" prefix="mytag"%>
  <style type="text/css">
    .block1 {
      background: #ccc;
      border: solid 1px black;
      float: left;
    }
  </style>
</head>
<body>
<%  ArrayList<String> ListUsers = new ArrayList();
  for(int i=1;i<=87;i++){
    ListUsers.add("User" + i);
  }
  int sizeList = ListUsers.size();
  request.setAttribute("ListUsers",ListUsers);
  String a;
  if((a = request.getParameter("page")) == null){
    a = "1";
  }

  String b;
  if((b = request.getParameter("perPage")) == null) {
    b = "10";
  }
  int firsElement = (Integer.parseInt(b)*(Integer.parseInt(a)-1)+1);
  int lastElement = (Integer.parseInt(b)*(Integer.parseInt(a)-1))+Integer.parseInt(b);
  if(lastElement>=ListUsers.size()){
    lastElement = ListUsers.size();
  }
  System.out.println(firsElement+"-"+lastElement);%>
<table border="5">
  <%for (int i=firsElement-1; i<lastElement; i++){%>
  <tr> <td><%= i+1%></td>
    <td><%= ListUsers.get(i)%></td> </tr>
  <%}%>
</table>
<form action="index.jsp" method="GET">
  <p>Enter the number of items per page </p>
  <input type="text" required  name="perPage">
  <input type="submit" value="ok">
</form>

<c:set var="namePage" value="<%= a%>" />
<c:set var="perPage" value="<%= b%>" />
<mytag:pageNavigation resultsPerPage="${perPage}" totalResults="${ListUsers.size()}" currentPage="${namePage}" />
</body>
</html>