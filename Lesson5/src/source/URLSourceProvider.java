package source;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Implementation for loading content from specified URL.<br/>
 * Valid paths to load are http://someurl.com, https://secureurl.com, ftp://frpurl.com etc.
 */
public class URLSourceProvider implements SourceProvider {

    @Override
    public boolean isAllowed(String pathToSource) {
        //Pattern pat = Pattern.compile("^(https?|ftp:\\/\\/)?([\\da-z\\.-]+)\\.([a-z\\.]{2,6})([\\/\\w \\.-]*)*\\/?$");
        Pattern pat = Pattern.compile("(?:https?|ftp)://[^\\s,<>\\?]+?\\.[^\\s,<>\\?]+");

        Matcher m = pat.matcher(pathToSource);
        if(m.find()){
            return true;
        }else {
            return false;
        }
    }

    @Override
    public String load(String pathToSource) throws IOException {

        BufferedReader in = new BufferedReader(new InputStreamReader(new URL(pathToSource).openStream()));
        String s;
        String s2= "";
        try {

            while ((s = in.readLine()) != null) {
                s2 =s2+s+"\n";
            }
        }catch (FileNotFoundException e) {
            System.err.println("Exception occurred:" + e.toString());
            e.printStackTrace();
        }catch (IOException e) {
            System.err.println("Exception occurred:" + e.toString());
            e.printStackTrace();
        }finally {
            if (in != null) in.close();
            return s2;
        }
    }
}
