package source;

import java.io.*;
import java.util.regex.Matcher;

/**
 * Implementation for loading content from local file system.
 * This implementation supports absolute paths to local file system without specifying file:// protocol.
 * Examples c:/1.txt or d:/pathToFile/file.txt
 */
public class FileSourceProvider implements SourceProvider {

    @Override
    public boolean isAllowed(String pathToSource) {
        File file = new File(pathToSource);
        if(file.exists() && file.isFile()){
            return true;
        }else {
            return false;
        }
    }

    @Override
    public String load(String pathToSource) throws IOException {
        BufferedReader in = new BufferedReader(new FileReader(pathToSource));;
        String s;
        String s2 = "";
        try {
            while ((s = in.readLine()) != null) {
                s2=s2+s+"\n";
            }
        } catch(FileNotFoundException e) {
            System.out.println(e);
        } catch (IOException e){
            System.err.println("Exception occurred:" + e.toString());
            e.printStackTrace();
        } finally {
            if (in != null) in.close();
            return s2;
        }
    }
}
