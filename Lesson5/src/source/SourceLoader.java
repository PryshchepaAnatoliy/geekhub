package source;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * SourceLoader should contains all implementations of SourceProviders to be able to load different sources.
 */
public class SourceLoader {
    private List<SourceProvider> sourceProviders = new ArrayList<SourceProvider>();
    String directory;
    SourceProvider sourceProvider;

    public SourceLoader() {
        //TODO: initialize me
    }

    public String loadSource(String pathToSource) throws IOException {
        System.out.println("Input type of source.(file/site)");
        Scanner sc = new Scanner(System.in);
        String typeSource = sc.next();

        System.out.println("Enter the address of the text.");
        Scanner sc1 = new Scanner(System.in);
        directory = sc1.next();
        if (typeSource.equals("file")){
            sourceProvider = new FileSourceProvider();
        }if (typeSource.equals("site")){
            sourceProvider = new URLSourceProvider();
        }else{
            System.out.println("Input type Error.");
        }
        if (sourceProvider.isAllowed(directory)) {
            return sourceProvider.load(directory);
        } else {
            throw new IOException();
        }
    }
}
